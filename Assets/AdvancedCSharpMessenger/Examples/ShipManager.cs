﻿using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour {

    List<FighterShip> _fighterShips;
    List<MotherShip> _motherShips;
    List<EnemyBullet> _enemyBullets;
    List<EnemyTower> _enemyTowers;

    void Update()
    {
        if (_motherShips.Count == 0)
            Messenger.Broadcast("mothership_died");
    }
}