﻿using System;
using UnityEngine;

public class EnemyTower : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    Messenger.AddListener("mothership_died", OnMotherShipDied);
	}

    void OnDestroy()
    {
        Messenger.RemoveListener("mothership_died", OnMotherShipDied);
    }

    private void OnMotherShipDied()
    {
        Die();
    }

    // Update is called once per frame
	void Update () {
	
	}

    public void Die()
    {
        throw new NotImplementedException();
    }
}
