﻿using System;
using UnityEngine;
using System.Collections;

public class FighterShip : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        Messenger.AddListener("mothership_died", OnMotherShipDied);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener("mothership_died", OnMotherShipDied);
    }

    private void OnMotherShipDied()
    {
        Die();
    }

    public void Die()
    {
        throw new NotImplementedException();
    }
}
