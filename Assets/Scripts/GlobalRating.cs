﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity3dAzure.AppServices;
using RestSharp;
using Pathfinding.Serialization.JsonFx;

public class GlobalRating : MonoBehaviour 
{

    int _gameNum;
    int _rating;

    private string _eventId;
    private string _uniqueId;
    private string _name;
    private string _email;

    private MobileServiceTable<Rating> _ratingTable;
    private MobileServiceTable<RatingHost> _ratingHostTable;

    public void RateGame(int gameRating, int gameNum, GameObject errorMessage = null)
    {
        if (gameRating > 0)
        {
            _gameNum = gameNum;
            _rating = gameRating;
            _eventId = GetComponent<GlobalInformation>().EventId;
            _uniqueId = GetComponent<GlobalInformation>().UniqueId;
            _name = GetComponent<GlobalInformation>().PlayerFirstName + " " + GetComponent<GlobalInformation>().PlayerLastName;
            _email = GetComponent<GlobalInformation>().PlayerEmail;

            _ratingTable = GetComponent<AppServiceManager>().RatingTable;

            string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

            CustomQuery query = new CustomQuery(filter);

            _ratingTable.Query<Rating>(query, RatingGame);
        }
    }

    void RatingGame(IRestResponse<List<Rating>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<Rating> list = response.Data;
            bool foundSelf = false;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId && item.Eventid == _eventId)
                {
                    item.RatingNum = _rating;
                    _ratingTable.Update<Rating>(item);
                    foundSelf = true;
                    break;
                }
            }

            if (foundSelf == false)
            {
                Rating player = new Rating();

                player.Eventid = _eventId;
                player.RatingNum = _rating;
                player.UniqueId = _uniqueId;
                player.Name = _name;
                player.Email = _email;

                _ratingTable.Insert<Rating>(player);
            }
        }
    }

    public void RateGameHost(int gameRating, int gameNum, GameObject errorMessage = null)
    {
        if (gameRating > 0)
        {
            _gameNum = gameNum;
            _rating = gameRating;
            _eventId = GetComponent<GlobalInformation>().EventId;
            _uniqueId = GetComponent<GlobalInformation>().UniqueId;
            _name = GetComponent<GlobalInformation>().PlayerFirstName + " " + GetComponent<GlobalInformation>().PlayerLastName;
            _email = GetComponent<GlobalInformation>().PlayerEmail;

            _ratingHostTable = GetComponent<AppServiceManager>().RatingHostTable;

            string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

            CustomQuery query = new CustomQuery(filter);

            _ratingHostTable.Query<RatingHost>(query, RatingGameHost);
        }
    }

    void RatingGameHost(IRestResponse<List<RatingHost>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<RatingHost> list = response.Data;
            bool foundSelf = false;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId && item.Eventid == _eventId)
                {
                    item.RatingNum = _rating;
                    _ratingHostTable.Update<RatingHost>(item);
                    foundSelf = true;
                    break;
                }
            }

            if (foundSelf == false)
            {
                RatingHost player = new RatingHost();

                player.Eventid = _eventId;
                player.RatingNum = _rating;
                player.UniqueId = _uniqueId;
                player.Name = _name;
                player.Email = _email;

                _ratingHostTable.Insert<RatingHost>(player);
            }
        }
    }
}
