﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICameraManager : MonoBehaviour {

    [SerializeField] private Camera _mainCamera;

	// Use this for initialization
	void Start () 
    {
        //This script will find the UI Camera and attach it to the newly made canvas.  This will fix any resizing issues

        _mainCamera = GameObject.Find("UI Camera").GetComponent<Camera>();
        GetComponent<Canvas>().worldCamera = _mainCamera;
	}
	
}
