﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GlobalInformation : MonoBehaviour 
{
    public string PlayerFirstName = ""; 
    public string PlayerLastName = ""; 
    public string PlayerEmail = ""; 
    public string UniqueId = null;
    public string EventId;
    public string EventName;

    public bool IsHost = false;
    public bool CheckedForInfo = false;
    public bool InGame = false;
    public bool CameFromGame = false;

    void Awake()
    {
        Application.targetFrameRate = 30;
    }

    void Start()
    {
        if (PlayerPrefs.HasKey("FirstName"))
        {
            PlayerFirstName = PlayerPrefs.GetString("FirstName");
            PlayerLastName = PlayerPrefs.GetString("LastName");
            PlayerEmail = PlayerPrefs.GetString("Email");
            EventId = PlayerPrefs.GetString("roomcode");
            if (PlayerPrefs.GetString("IsHost") == "true")
            {
                IsHost = true;
            }
            EventName = PlayerPrefs.GetString("EventName");
        }

        if (PlayerPrefs.HasKey("PlayerAssignedId"))
        {
            UniqueId = PlayerPrefs.GetString("PlayerAssignedId");
        }
    }
}
