﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// The CanvasManager script should reside at the root on every Canvas object
// It is used to handle any buttons clicked on the Canvas, and then notify the MenuStateManager of these events
// Make sure that every button that either goes back, or clicks through to a new menu, is handled in this script


// Feel free to add additional buttons to the code if they don't exist yet.
public class CanvasManager : MonoBehaviour
{
    //1).
    // Make sure the relevant buttons are set on the CanvasManager script in the editor for the Canvas you are working with
    // Not every button needs to be assigned, only assign buttons that exist on that Canvas
    [Space (2)]
    [Header ("Unused Buttons\n")]
    [SerializeField] private Button _backButton;
    [SerializeField] private MenuState _backButtonTarget;
    [SerializeField] private Button _profileButton;
    [SerializeField] private Button _hostProfileButton;
    [Space(2)]
    [Header("Event Screens\n")]
    [SerializeField] private Button _homeButton;
    [SerializeField] private Button _codeButton;
    [SerializeField] private Button _surveyButton;
    [Space(2)]
    [Header("Meshworking Screens\n")]
    [SerializeField] private Button _socialScavengerHomeButton;
    [SerializeField] private Button _socialScavengerGameBoardButton;
    [SerializeField] private Button _meshworkingHostButton;

    private string _broadcastString;

    private bool _isTransitioning = false;

    public Object[] CanvasCheck;

    void Awake()
    {
        CanvasCheck = GameObject.FindObjectsOfType(typeof(Canvas));
    }

    void Start()
    {

        if (CanvasCheck[0].name == CanvasCheck[1].name && CanvasCheck.Length > 3)
        {
            GameObject.Destroy(gameObject);
        }

        //2).
        // Subscribe to this event so when a close_all_menus event is broadcast, we know to destroy this menu
        Messenger.AddListener("close_all_menus", CloseMenu);
        Messenger.AddListener("done_transitioning", DoneTransitioning);

        if (_backButton != null) _backButton.onClick.AddListener(OnBackButtonClicked);
        if (_homeButton != null) _homeButton.onClick.AddListener(OnHomeButtonClicked);
        if (_codeButton != null) _codeButton.onClick.AddListener(OnCodeButtonClicked);
        if (_socialScavengerHomeButton != null) _socialScavengerHomeButton.onClick.AddListener(OnSocialScavengerHomeButtonClicked);
        if (_socialScavengerGameBoardButton != null) _socialScavengerGameBoardButton.onClick.AddListener(OnSocialScavengerGameBoardButtonClicked);
        if (_profileButton != null) _profileButton.onClick.AddListener(OnProfileButtonClicked);
        if (_hostProfileButton != null) _hostProfileButton.onClick.AddListener(OnHostProfileButtonClicked);
        if (_meshworkingHostButton != null) _meshworkingHostButton.onClick.AddListener(OnMeshworkingHostButtonClicked);
        if (_surveyButton != null) _surveyButton.onClick.AddListener(OnSurveyButtonClicked);

    }

    void OnDestroy()
    {
        //3).
        Messenger.RemoveListener("close_all_menus", CloseMenu);
        Messenger.RemoveListener("done_transitioning", DoneTransitioning);

        if (_backButton != null) _backButton.onClick.RemoveListener(OnBackButtonClicked);
        if (_homeButton != null) _homeButton.onClick.RemoveListener(OnHomeButtonClicked);
        if (_codeButton != null) _codeButton.onClick.RemoveListener(OnCodeButtonClicked);
        if (_socialScavengerHomeButton != null) _socialScavengerHomeButton.onClick.RemoveListener(OnSocialScavengerHomeButtonClicked);
        if (_socialScavengerGameBoardButton != null) _socialScavengerGameBoardButton.onClick.RemoveListener(OnSocialScavengerGameBoardButtonClicked);
        if (_profileButton != null) _profileButton.onClick.RemoveListener(OnProfileButtonClicked);
        if (_hostProfileButton != null) _hostProfileButton.onClick.RemoveListener(OnHostProfileButtonClicked);
        if (_meshworkingHostButton != null) _meshworkingHostButton.onClick.RemoveListener(OnMeshworkingHostButtonClicked);
        if (_surveyButton != null) _surveyButton.onClick.RemoveListener(OnSurveyButtonClicked);

    }

    // Go back to the previous screen
    public void OnBackButtonClicked()
    {
        if (_backButtonTarget == MenuState.None)
        {
            Debug.Log("Quitting application");
            Application.Quit();
        }
    }
    
    
    //4).

    public void OnMeshworkingHostButtonClicked()
    {
        Messenger.Broadcast("meshworking_host_button_clicked");
    }

    public void OnHomeButtonClicked()
    {
        Messenger.Broadcast("home_button_clicked");
    }

    public void OnCodeButtonClicked()
    {
        Messenger.Broadcast("code_button_clicked");
    }

    public void OnSocialScavengerHomeButtonClicked()
    {
        Messenger.Broadcast("socialscavenger_home_button_clicked");
    }

    public void OnSocialScavengerGameBoardButtonClicked()
    {
       Messenger.Broadcast("socialscavenger_gameBoard_button_clicked");
    }

    public void OnProfileButtonClicked()
    {
        Messenger.Broadcast("profile_button_clicked");
    }

    public void OnHostProfileButtonClicked()
    {
        Messenger.Broadcast("host_profile_button_clicked");
    }

    public void OnSurveyButtonClicked()
    {
        Messenger.Broadcast("survey_button_clicked");
    }

    private void CloseMenu()
    {
        // Don't ever destroy the main menu base canvas
        if (gameObject.name == "Canvas_Base")
            return;

        // Now destroy this menu
        Destroy(this.gameObject);
    }

    IEnumerator WaitForTransition(string broadcastString)
    {
        while (_isTransitioning)
        {
            yield return null;
        }

        Messenger.Broadcast(broadcastString);
    }

    void DoneTransitioning()
    {
        _isTransitioning = false;
    }
}