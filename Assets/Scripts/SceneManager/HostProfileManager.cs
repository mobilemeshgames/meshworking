﻿using UnityEngine;
using System;
using System.Net;
using System.Collections.Generic;
using RestSharp;
using Pathfinding.Serialization.JsonFx;
using Unity3dAzure.AppServices;
using UnityEngine.UI;

public class HostProfileManager : MonoBehaviour 
{
    private GameObject GameInformationObject;

    [SerializeField] private Button _submitButton;

    [SerializeField] private InputField _eventInput;
    [SerializeField] private InputField _firstNameInput;
    [SerializeField] private InputField _lastNameInput;
    [SerializeField] private InputField _emailInput;

    private string _firstName;
    private string _lastName;
    private string _eventName;
    private string _email;
    private string _generatedRoom;
    private string _uniqueId;
    private string _eventId;

    private bool _createdEvent = false;

    private List<string> _stringList = new List<string>();

    private MobileServiceTable<MeshworkingTable> _table;

    void Start()
    {
        for (int i = 1; i < 10; i++)
        {
            _stringList.Add(i.ToString());
        }

        char[] alpha = "ABCDEFGHIJKLMNPQRSTUVWXYZ".ToCharArray();

        for (int i = 0; i < alpha.Length; i++)
        {
            _stringList.Add(alpha[i].ToString());
        }

        GameInformationObject = GameObject.Find("GameInformation");

        _table = GameInformationObject.GetComponent<AppServiceManager>().Table;

        _firstName = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName;
        _lastName = GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;
        _email = GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail;
        _uniqueId = GameInformationObject.GetComponent<GlobalInformation>().UniqueId;

        if (_firstName != null && _firstName != "")
        {
            _firstNameInput.text = _firstName;
        }

        if (_lastName != null && _lastName != "")
        {
            _lastNameInput.text = _lastName;
        }

        if (_email != null && _email != "")
        {
            _emailInput.text = _email;
        }

        _eventInput.onValueChanged.AddListener(delegate
        {
            _eventName = _eventInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "" && _eventName != null && _eventName != "" && _email != null && _email != "")
            {
                _submitButton.interactable = true;
            }
            else
            {
                _submitButton.interactable = false;
            }
        });

        _firstNameInput.onValueChanged.AddListener(delegate
        {
            _firstName = _firstNameInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "" && _eventName != null && _eventName != "" && _email != null && _email != "")
            {
                _submitButton.interactable = true;
            }
            else
            {
                _submitButton.interactable = false;
            }
        });

        _lastNameInput.onValueChanged.AddListener(delegate 
        { 
            _lastName = _lastNameInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "" && _eventName != null && _eventName != "" && _email != null && _email != "")
            {
                _submitButton.interactable = true;
            }
            else
            {
                _submitButton.interactable = false;
            }
        });

        _emailInput.onValueChanged.AddListener(delegate
        {
            _email = _emailInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "" && _eventName != null && _eventName != "" && _email != null && _email != "")
            {
                _submitButton.interactable = true;
            }
            else
            {
                _submitButton.interactable = false;
            }
        });

        _submitButton.onClick.AddListener(() =>
        {
            GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName = _firstName;
            GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName = _lastName;
            GameInformationObject.GetComponent<GlobalInformation>().EventName = _eventName;
            GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail = _emailInput.text;

            PlayerPrefs.SetString("FirstName", _firstName);
            PlayerPrefs.SetString("LastName", _lastName);
            PlayerPrefs.SetString("Email", _emailInput.text);
            PlayerPrefs.SetString("EventName", _eventName);

            CreateEvent();
        });
    }

    void Update()
    {
        if (_createdEvent)
        {
            _createdEvent = false;

            GameInformationObject.GetComponent<GlobalInformation>().UniqueId = _uniqueId;
            GameInformationObject.GetComponent<GlobalInformation>().EventId = _generatedRoom;
            GameInformationObject.GetComponent<GlobalInformation>().IsHost = true;

            PlayerPrefs.SetString("PlayerAssignedId", _uniqueId);
            PlayerPrefs.SetString("roomcode", _eventId);
            PlayerPrefs.SetString("IsHost", "true");
            PlayerPrefs.SetString("EventName", _eventName);

            GetComponent<CanvasManager>().OnHomeButtonClicked();
        }
    }

    void CreateEvent()
    {
        _submitButton.interactable = false;
        _generatedRoom = "";

        for (int i = 0; i < 4; i++)
        {
            _generatedRoom += _stringList[UnityEngine.Random.Range(0, _stringList.Count)];
        }

        string filter = string.Format("eventid eq '{0}'", _generatedRoom);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, CreatingEvent); //Query(query);
    }

    void CreatingEvent(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            bool match = false;
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (_generatedRoom == item.EventId)
                {
                    match = true;
                    break;
                }
            }

            if (match == false)
            {
                MeshworkingTable player = new MeshworkingTable
                {
                    UniqueId = Guid.NewGuid().ToString(),
                    EventId = _generatedRoom,
                    IsHost = true,
                    StartGame = false,
                    Name = _firstName + " " + _lastName,
                    Email = _email,
                    EventName = _eventInput.text,
                    InGame = false
                };

                _uniqueId = player.UniqueId;
                _eventId = _generatedRoom;

                _table.Insert<MeshworkingTable>(player);

                _createdEvent = true;
            }
            else
            {
                CreateEvent();
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }
    }
}
