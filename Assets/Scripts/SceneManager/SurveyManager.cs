﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity3dAzure.AppServices;
using RestSharp;
using Pathfinding.Serialization.JsonFx;

public class SurveyManager : MonoBehaviour 
{
    private GameObject GameInformationObject;
    [SerializeField] private GameObject _thanksPanel;

    [SerializeField] private Text _questionCountText;
    [SerializeField] private Text _questionText;
    [SerializeField] private Text _answer1Text;
    [SerializeField] private Text _answer2Text;
    [SerializeField] private Text _answer3Text;
    [SerializeField] private Text _answer4Text;
    [SerializeField] private Text _answer5Text;

    
    [SerializeField] private Button _answer1Button;
    [SerializeField] private Button _answer2Button;
    [SerializeField] private Button _answer3Button;
    [SerializeField] private Button _answer4Button;
    [SerializeField] private Button _answer5Button;
    [SerializeField] private Button _yesButton;
    [SerializeField] private Button _noButton;
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _submitButton;
    [SerializeField] private Button _moreActivitiesButton;

    [SerializeField] private InputField _shortAnswer;

    [SerializeField] private List<string> _questionList = new List<string>();
    [SerializeField] private List<string> _answer1List = new List<string>();
    [SerializeField] private List<string> _answer2List = new List<string>();
    [SerializeField] private List<string> _answer3List = new List<string>();
    [SerializeField] private List<List<string>> _answerHolder = new List<List<string>>();

    private int _questionCount = 1;

    private string _eventId;
    private string _uniqueId;
    private string _name;
    private string _email;
    private string _answer1;
    private string _answer2;
    private string _answer3;
    private string _tempAnswer;

    private bool _finishedSurvey = false;
    private bool _submittingAnswers = false;

    private MobileServiceTable<Survey> _table;

	// Use this for initialization
	void Start ()
    {
        GameInformationObject = GameObject.Find("GameInformation");

        _eventId = GameInformationObject.GetComponent<GlobalInformation>().EventId;
        _name = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName + " " + GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;
        _email = GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail;
        _uniqueId = GameInformationObject.GetComponent<GlobalInformation>().UniqueId;

        _table = GameInformationObject.GetComponent<AppServiceManager>().SurveyTable;

        _questionList.Add("Would you recommend this activity to someone else wanting to do a networking icebreaker activity?");
        _questionList.Add("Given the opportunity, would you participate in another Meshworking activity in the future?");
        _questionList.Add("Provide additional comments or questions.");

        _answer1List.Add("Excellent");
        _answer1List.Add("Very Good");
        _answer1List.Add("Good");
        _answer1List.Add("Fair");
        _answer1List.Add("Poor");

        _answerHolder.Add(_answer1List);

        _answer2List.Add("Yes");
        _answer2List.Add("No");

        _answerHolder.Add(_answer2List);

        _answer3List.Add("Extremely Effective");
        _answer3List.Add("Very Effective");
        _answer3List.Add("Somewhat Effective");
        _answer3List.Add("Not so Effective");
        _answer3List.Add("Not at all Effective");

        _answerHolder.Add(_answer3List);

        _nextButton.onClick.AddListener(NextQuestion);
        _submitButton.onClick.AddListener(() => 
        {
            _submittingAnswers = true;
            _submitButton.interactable = false;
            _answer3 = _shortAnswer.text;
            SubmitAnswer();
        });

        _moreActivitiesButton.onClick.AddListener(() => { Application.OpenURL("http://mobilemeshevents.com/activities"); });

        _questionCountText.text = "Question " + _questionCount + " of " + _questionList.Count;

        _yesButton.onClick.AddListener(() =>
        {
            _tempAnswer = "Yes";
            NextQuestion();
        });

        _noButton.onClick.AddListener(() =>
        {
                _tempAnswer = "No";
                NextQuestion();
        });

        _answer1Button.onClick.AddListener(() => SelectAnswer(1));
        _answer2Button.onClick.AddListener(() => SelectAnswer(2));
        _answer3Button.onClick.AddListener(() => SelectAnswer(3));
        _answer4Button.onClick.AddListener(() => SelectAnswer(4));
        _answer5Button.onClick.AddListener(() => SelectAnswer(5));

        _questionText.text = _questionList[_questionCount - 1];

    }

    void Update()
    {
        if (_finishedSurvey)
        {
            _finishedSurvey = false;
            _thanksPanel.SetActive(true);
        }
    }

    void NextQuestion()
    {
        
        switch (_questionCount)
        {
            case 1:
                _answer1 = _tempAnswer;
                break;
            case 2:
                _answer2 = _tempAnswer;
                break;
            case 3:
                _answer3 = _tempAnswer;
                break;
        }

        //ClearButtons();

        _questionCount++;

        if (_questionCount <= _questionList.Count)
        {
            _questionCountText.text = "Question " + _questionCount + " of " + _questionList.Count;

            _questionText.text = _questionList[_questionCount - 1];
/*
            _answer1Text.text = _answerHolder[_questionCount - 1][0];
            _answer2Text.text = _answerHolder[_questionCount - 1][1];
            if (_answerHolder[_questionCount - 1].Count >= 3)
            {
                _answer3Text.text = _answerHolder[_questionCount - 1][2];
                _answer3Button.gameObject.SetActive(true);
            }
            else
            {
                _answer3Button.gameObject.SetActive(false);
            }

            if (_answerHolder[_questionCount - 1].Count >= 4)
            {
                _answer4Text.text = _answerHolder[_questionCount - 1][3];
                _answer4Button.gameObject.SetActive(true);
            }
            else
            {
                _answer4Button.gameObject.SetActive(false);
            }

            if (_answerHolder[_questionCount - 1].Count == 5)
            {
                _answer5Text.text = _answerHolder[_questionCount - 1][4];
                _answer5Button.gameObject.SetActive(true);
            }
            else
            {
                _answer5Button.gameObject.SetActive(false);
            }*/

            if (_questionCount == _questionList.Count)
            {
                _nextButton.gameObject.SetActive(false);
                _submitButton.gameObject.SetActive(true);

                _answer1Button.gameObject.SetActive(false);
                _answer2Button.gameObject.SetActive(false);
                _answer3Button.gameObject.SetActive(false);
                _answer4Button.gameObject.SetActive(false);
                _answer5Button.gameObject.SetActive(false);
                _yesButton.gameObject.SetActive(false);
                _noButton.gameObject.SetActive(false);

                _shortAnswer.gameObject.SetActive(true);
            }

            SubmitAnswer();

            _nextButton.interactable = false;
            _tempAnswer = "";
        }
    }

    void SelectAnswer(int AnswerNum)
    {
        ClearButtons();

        switch (AnswerNum)
        {
            case 1:
                _answer1Button.transform.GetChild(0).gameObject.SetActive(true);
                _tempAnswer = _answer1Button.transform.GetChild(2).GetComponent<Text>().text;
                break;
            case 2:
                _answer2Button.transform.GetChild(0).gameObject.SetActive(true);
                _tempAnswer = _answer2Button.transform.GetChild(2).GetComponent<Text>().text;
                break;
            case 3:
                _answer3Button.transform.GetChild(0).gameObject.SetActive(true);
                _tempAnswer = _answer3Button.transform.GetChild(2).GetComponent<Text>().text;
                break;
            case 4:
                _answer4Button.transform.GetChild(0).gameObject.SetActive(true);
                _tempAnswer = _answer4Button.transform.GetChild(2).GetComponent<Text>().text;
                break;
            case 5:
                _answer5Button.transform.GetChild(0).gameObject.SetActive(true);
                _tempAnswer = _answer5Button.transform.GetChild(2).GetComponent<Text>().text;
                break;
        }

        _nextButton.interactable = true;
    }

    void ClearButtons()
    {
        _answer1Button.transform.GetChild(0).gameObject.SetActive(false);
        _answer2Button.transform.GetChild(0).gameObject.SetActive(false);
        _answer3Button.transform.GetChild(0).gameObject.SetActive(false);
        _answer4Button.transform.GetChild(0).gameObject.SetActive(false);
        _answer5Button.transform.GetChild(0).gameObject.SetActive(false);
    }

    void SubmitAnswer()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<Survey>(query, SubmittingAnswer);
    }

    void SubmittingAnswer(IRestResponse<List<Survey>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<Survey> list = response.Data;
            bool _foundSelf = false;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    _foundSelf = true;

                    item.UniqueId = _uniqueId;
                    item.Email = _email;
                    item.Name = _name;
                    item.Eventid = _eventId;
                    item.Question1 = _answer1;
                    item.Question2 = _answer2;
                    item.Question3 = _answer3;

                    _table.Update<Survey>(item);
                    break;
                }
            }

            if (!_foundSelf)
            {
                Survey player = new Survey();
                player.UniqueId = _uniqueId;
                player.Email = _email;
                player.Name = _name;
                player.Eventid = _eventId;
                player.Question1 = _answer1;
                player.Question2 = _answer2;
                player.Question3 = _answer3;

                _table.Insert<Survey>(player);
            }
        }

        if (_submittingAnswers)
        {
            _finishedSurvey = true;
        }
    }
}
