﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity3dAzure.AppServices;
using RestSharp;
using Pathfinding.Serialization.JsonFx;

public class MeshworkingHostManager : MonoBehaviour
{
    
    private GameObject GameInformationObject;

    private string _uniqueId;
    private string _eventId;

    private bool _singleUse = true;
    private bool _startGame = false;
    private bool _populatePlayerList = false;
    private bool _startRefreshing = true;
    private bool _refreshOnce = false;
    private bool _startTimer = false;
    private bool _deletedEvent = false;

    private DateTime _startTime;
    private TimeSpan _displayTime;
    private TimeSpan _totalTime;
    
    [SerializeField] private GameObject _participantBanner;
    [SerializeField] private GameObject _participantPanel;
    [SerializeField] private GameObject _participantPrefab;
    [SerializeField] private GameObject _participantHolder;
    [SerializeField] private GameObject _notesPanel;
    [SerializeField] private GameObject _loadingPanel;

    [SerializeField] private Button _backButton;
    [SerializeField] private Button _notesButton;
    [SerializeField] private Button _notesBackButton;
    [SerializeField] private Button _participantDropdownButton;
    [SerializeField] private Button _startActivityButton;
    [SerializeField] private Button _videoButton;

    [SerializeField] private Text _playerCountText;

    [Space (5)]
    [Header ("Configureation")]
    [Space(5)]
    [SerializeField] private GameObject _areYouSurePanel;
    [SerializeField] private GameObject _configParticipantHolder;
    [SerializeField] private GameObject _configParticipantPrefab;
    [SerializeField] private GameObject _deleteAreYouSure;
    [SerializeField] private GameObject _deletingPanel;
    [SerializeField] private Button _noButton;
    [SerializeField] private Button _yesButton;
    [SerializeField] private Button _deleteButton;
    [SerializeField] private Button _noDeleteButton;
    [SerializeField] private Button _yesDeleteButton;
    [SerializeField] private Text _eventNameText;
    [SerializeField] private Text _eventIdText;
    [SerializeField] private Text _participantCountText;

    [Space(5)]
    [Header("In Game")]
    [SerializeField] private GameObject _inGamePanel;
    [SerializeField] private Text _cardTotalText;
    [SerializeField] private Text _cardHighText; // "_cardHighText.text = "HIGH: " + _cardHighNumber;
    [SerializeField] private Text _cardAvgText; // "_cardAvgText.text = "AVG: " + _cardAvgNumber;
    [SerializeField] private Text _cardlowText; // "_cardLowText.text = "LOW: " + _cardLowNumber;
    [Space(5)]
    [SerializeField] private Text _firstPlaceName;
    [SerializeField] private Text _secondPlaceName;
    [SerializeField] private Text _thirdPlaceName;
    [SerializeField] private Text _fourthPlaceName;
    [SerializeField] private Text _fifthPlaceName;
    [SerializeField] private Text _fifthLastPlaceName;
    [SerializeField] private Text _fourthLastPlaceName;
    [SerializeField] private Text _thirdLastPlaceName;
    [SerializeField] private Text _secondLastPlaceName;
    [SerializeField] private Text _firstLastPlaceName;
    [Space(5)]
    [SerializeField] private Text _firstPlaceCardCount;
    [SerializeField] private Text _secondPlaceCardCount;
    [SerializeField] private Text _thirdPlaceCardCount;
    [SerializeField] private Text _fourthPlaceCardCount;
    [SerializeField] private Text _fifthPlaceCardCount;
    [SerializeField] private Text _fifthLastPlaceCardCount;
    [SerializeField] private Text _fourthLastPlaceCardCount;
    [SerializeField] private Text _thirdLastPlaceCardCount;
    [SerializeField] private Text _secondLastPlaceCardCount;
    [SerializeField] private Text _firstLastPlaceCardCount;
    [Space(5)]
    [SerializeField] private Text _fifthLastPlaceOrderNumber;
    [SerializeField] private Text _fourthLastPlaceOrderNumber;
    [SerializeField] private Text _thirdLastPlaceOrderNumber;
    [SerializeField] private Text _secondLastPlaceOrderNumber;
    [SerializeField] private Text _firstLastPlaceOrderNumber;
    [Space(5)]
    [SerializeField] private Text _timerText;
    [SerializeField] private Button _endActivityButton;
    [Space(5)]
    [SerializeField] private GameObject _areYouSureEndEventPanel;
    [SerializeField] private Button _noEndEventButton;
    [SerializeField] private Button _yesEndEventButton;

    [SerializeField] private List<GameObject> TopPlayers = new List<GameObject>();
    [SerializeField] private List<GameObject> BottomPlayers = new List<GameObject>();

    private int _cardTotalNumber = 0;
    private int _cardHighNumber = 0;
    private int _cardAvgNumber = 0;
    private int _cardLowNumber = 0;
    private int _refreshRate = 5;
    private int _starRating = 0;

    [Space(5)]
    [Header("End Game")]
    [SerializeField] private GameObject _endGamePanel;
    [SerializeField] private GameObject _pleaseRatePanel;
    [SerializeField] private Text _endCardTotalText;
    [SerializeField] private Text _endCardHighAvgLowText; // "_cardHighText.text = "HIGH: " + _cardHighNumber + "     AVG: " + _cardAvgNumber + "     LOW: " + _cardLowNubmer;
    [Space(5)]
    [SerializeField] private Text _endFirstPlaceName;
    [SerializeField] private Text _endSecondPlaceName;
    [SerializeField] private Text _endThirdPlaceName;
    [SerializeField] private Text _endThirdLastPlaceName;
    [SerializeField] private Text _endSecondLastPlaceName;
    [SerializeField] private Text _endFirstLastPlaceName;
    [Space(5)]
    [SerializeField] private Text _endFirstPlaceCardCount;
    [SerializeField] private Text _endSecondPlaceCardCount;
    [SerializeField] private Text _endThirdPlaceCardCount;
    [SerializeField] private Text _endThirdLastPlaceCardCount;
    [SerializeField] private Text _endSecondLastPlaceCardCount;
    [SerializeField] private Text _endFirstLastPlaceCardCount;
    [Space(5)]
    [SerializeField] private Text _endThirdLastPlaceOrderNumber;
    [SerializeField] private Text _endSecondLastPlaceOrderNumber;
    [SerializeField] private Text _endFirstLastPlaceOrderNumber;
    [Space(5)]
    [SerializeField] private Button _star1;
    [SerializeField] private Button _star2;
    [SerializeField] private Button _star3;
    [SerializeField] private Button _star4;
    [SerializeField] private Button _star5;
    [SerializeField] private Button _closePleaseRateButton;
    [SerializeField] private Button _exitButton;
    [Space(5)]
    [SerializeField] private Sprite _activeStar;
    [SerializeField] private Sprite _inactiveStar;

    [SerializeField] private List<GameObject> Top3Players = new List<GameObject>();
    [SerializeField] private List<GameObject> Bottom3Players = new List<GameObject>();

    private Color32 _lightGreyColor = new Color32(191, 191, 191, 255);
    private Color32 _greyColor = new Color32(147, 147, 147, 255);
    private Color32 _orangeColor = new Color32(245, 141, 32, 255);
    private Color32 _purpleColor = new Color32(97, 42, 130, 255);

    private MeshworkingTable player = new MeshworkingTable();
    //private List<MasterProfile> _globalPlayerList = new List<MasterProfile>();
    private List<MeshworkingTable> _localPlayerList = new List<MeshworkingTable>();

    private MobileServiceTable<MeshworkingTable> _table;

    private uint _skip = 0; // no of records to skip
    private uint _totalCount = 0; // count value should be > 0 to paginate

    private bool _isPaginated = false; // only enable infinite scrolling for paginated results
    private bool HasNewData = false; // to reload table view when data has changed
    private bool _isLoadingNextPage = false; // load once when scroll buffer is hit

    void Start()
    {
        GameInformationObject = GameObject.Find("GameInformation");
        _uniqueId = GameInformationObject.GetComponent<GlobalInformation>().UniqueId;
        _eventId = GameInformationObject.GetComponent<GlobalInformation>().EventId;

        _eventIdText.text = _eventId;

        _table = GameInformationObject.GetComponent<AppServiceManager>().Table;

        _eventNameText.text = GameInformationObject.GetComponent<GlobalInformation>().EventName;

        player.UniqueId = _uniqueId;
        player.EventId = _eventId;
        player.Email = GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail;
        player.Name = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName + " " + GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;
        player.IsHost = true;
        player.CardsCompleted = 0;
        player.PhotosTaken = 0;

        /*for (int i = 0; i < 2; i++)
        {
            Meshworking player2 = new Meshworking();
            player2.Name = "Player " + i;
            player2.Photostaken = 6;
            player2.EventId = "test";
            player2.UnityId = Guid.NewGuid().ToString();
            player2.UsePersonOnce = false;
            player2.CardsCompleted = UnityEngine.Random.Range(0, 1000);

            AzureInit.Instance.azure.Insert<Meshworking>(player2);
        }*/

        _participantDropdownButton.onClick.AddListener(() =>
        {
            if (_participantPanel.activeSelf == true)
            {
                _participantPanel.SetActive(false);
            }
            else
            {
                _participantPanel.SetActive(true);
            }
        });

        OpenLobby();

        _backButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(true);
        });

        _noButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(false);
        });

        _startActivityButton.onClick.AddListener(StartMeshworking);

        _notesButton.onClick.AddListener(() => _notesPanel.SetActive(true));
        _notesBackButton.onClick.AddListener(() => _notesPanel.SetActive(false));

        _endActivityButton.onClick.AddListener(() => _areYouSureEndEventPanel.SetActive(true));

        _noEndEventButton.onClick.AddListener(() => _areYouSureEndEventPanel.SetActive(false));
        _yesEndEventButton.onClick.AddListener(() =>
        {
            _startGame = false;
            _startRefreshing = false;
            _startTimer = false;
            GetOrder();
            _areYouSureEndEventPanel.SetActive(false);
            _endGamePanel.SetActive(true);
            EndGame();
        });

        _star1.onClick.AddListener(() => StarChanged(1));
        _star2.onClick.AddListener(() => StarChanged(2));
        _star3.onClick.AddListener(() => StarChanged(3));
        _star4.onClick.AddListener(() => StarChanged(4));
        _star5.onClick.AddListener(() => StarChanged(5));

        _exitButton.onClick.AddListener(() =>
        {
            if (_starRating > 0)
            {
                GameInformationObject.GetComponent<GlobalRating>().RateGameHost(_starRating, 2);
                GetComponent<CanvasManager>().OnSurveyButtonClicked();
            }
            else
            {
                _pleaseRatePanel.SetActive(true);
            }
        });

        _closePleaseRateButton.onClick.AddListener(() => _pleaseRatePanel.SetActive(false));

        _videoButton.onClick.AddListener(() => Application.OpenURL("http://www.mobilemeshevents.com/explainer"));

        _deleteButton.onClick.AddListener(() => _deleteAreYouSure.SetActive(true));

        _yesDeleteButton.onClick.AddListener(() => 
        {
            _deletingPanel.SetActive(true);
            DeleteEvent();
        });

        _noDeleteButton.onClick.AddListener(() => _deleteAreYouSure.SetActive(false));
    }

    void Update()
    {
        if (_startGame)
        {
            _participantBanner.gameObject.SetActive(true);
            _inGamePanel.SetActive(true);
            _backButton.gameObject.SetActive(false);
        }

        if (_startRefreshing)
        {
            if (Time.time % _refreshRate >= 0 && Time.time % _refreshRate <= .5f)
            {
                if (_refreshOnce)
                {
                    GetPlayerList();

                    if (_startGame)
                    {
                        GetOrder();
                    }

                    _refreshOnce = false;
                }
            }
            else if (Time.time % _refreshRate > .5f)
            {
                _refreshOnce = true;
            }
        }

        if (_populatePlayerList)
        {
            _populatePlayerList = false;
            int _numCount = 0;

            foreach (Transform child in _participantHolder.transform)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in _configParticipantHolder.transform)
            {
                Destroy(child.gameObject);
            }

            //Fill playerlist here;
            if (_localPlayerList.Count > 0)
            {
                _startActivityButton.interactable = true;
                foreach (var player in _localPlayerList)
                {
                    var newPlayer = Instantiate(_participantPrefab, _participantHolder.transform.localPosition, _participantPrefab.transform.rotation) as GameObject;

                    newPlayer.transform.SetParent(_participantHolder.transform);
                    newPlayer.transform.localScale = Vector3.one;

                    newPlayer.GetComponentInChildren<Text>().text = player.Name;
                    
                    if (player.InGame == false)
                    {
                        newPlayer.transform.GetChild(1).gameObject.SetActive(true);
                        newPlayer.transform.SetSiblingIndex(0);
                    }
                    else
                    {
                        _numCount++;
                    }

                    var newPlayer2 = Instantiate(_configParticipantPrefab, _configParticipantHolder.transform.localPosition, _configParticipantPrefab.transform.rotation) as GameObject;

                    newPlayer2.transform.SetParent(_configParticipantHolder.transform);
                    newPlayer2.transform.localScale = Vector3.one;

                    newPlayer2.GetComponentInChildren<Text>().text = player.Name;
                }
            }
            
            if (_numCount > 0)
            {
                _playerCountText.text = _numCount + "/" + _localPlayerList.Count;
            }
            else
            {
                _playerCountText.text = "0/" + _localPlayerList.Count;
            }
            //_playerCountText.text = _localPlayerList.Count.ToString();

            _participantCountText.text = _localPlayerList.Count.ToString();
        }

        if (_startTimer)
        {
            _displayTime = (DateTime.Now - _startTime);

            if (_displayTime >= TimeSpan.Zero)
            {
                if (_displayTime.Seconds > 9)
                {
                    _timerText.text = _displayTime.Minutes + ":" + _displayTime.Seconds;
                }
                else
                {
                    _timerText.text = _displayTime.Minutes + ":0" + _displayTime.Seconds;
                }
            }
        }

        if (_deletedEvent)
        {
            _deletedEvent = false;

            GameInformationObject.GetComponent<GlobalInformation>().IsHost = false;
            GameInformationObject.GetComponent<GlobalInformation>().EventId = "";
            GameInformationObject.GetComponent<GlobalInformation>().EventName = "";

            PlayerPrefs.SetString("IsHost", "false");
            PlayerPrefs.SetString("roomcode", "");
            PlayerPrefs.SetString("EventName", "");

            GetComponent<CanvasManager>().OnHomeButtonClicked();
        }
    }

    void OpenLobby()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, OpeningLobby);
    }

    void OpeningLobby(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    item.UniqueId = _uniqueId;
                    item.Email = player.Email;
                    item.Name = player.Name;
                    item.EventName = _eventNameText.text;

                    player = item;

                    _table.Update<MeshworkingTable>(item);
                    break;
                }
            }
        }
    }

    void StartMeshworking()
    {
        _loadingPanel.SetActive(true);

        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, StartingMeshworking);
    }

    public void StartingMeshworking(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            //_globalPlayerList.Clear();

            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId != _uniqueId)
                {
                    //_globalPlayerList.Add(item);
                }
                else
                {
                    item.StartGame = true;
                    item.InGame = true;
                    _table.Update<MeshworkingTable>(item);
                    _startGame = true;
                    _startTimer = true;
                    _startTime = DateTime.Now;
                }
            }
        }        
    }

    //void GetPlayerList()
    //{
        /*
        var query = OQuery.From("MasterProfile")
            .Let("PartId", _roomNum)
            .Where("item => item.PartId == $PartId");

        AzureInit.Instance.azure.Where<MasterProfile>(query, GettingGlobalPlayerList);*/

    //    string filter = string.Format("eventid eq '{0}'", _eventId);
//
//        CustomQuery query = new CustomQuery(filter);

  //      _table.Query<MeshworkingTable>(query, GettingLocalPlayerList);
 //   }

    void GettingGlobalPlayerList(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            //_globalPlayerList.Clear();

            List<MeshworkingTable> list = response.Data;
            /*
            foreach (var item in list)
            {
                if (item.UnityId != null && item.HostId == null)
                {
                    _globalPlayerList.Add(item);
                }
            }*/
        }
    }

/*    void GettingLocalPlayerList(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;
            _localPlayerList.Clear(); 
            
            foreach (var item in list)
            {
                if (item.UniqueId != null && item.IsHost == false)
                {
                    _localPlayerList.Add(item);
                }
            }
            _populatePlayerList = true;
        }
    }*/

    void GetOrder()
    {
        int _count = _localPlayerList.Count;

        int[] temp = new int[_count];

        for (int n = 0; n < _count; n++)
        {
            temp[n] = n;
        }

        for (int j = 0; j < _count; j++)
        {
            for (int i = 0; i < _count; i++)
            {
                if (i < _count - 1)
                {
                    if (_localPlayerList[temp[i]].CardsCompleted < _localPlayerList[temp[i + 1]].CardsCompleted)
                    {
                        int temp2;
                        temp2 = temp[i];
                        temp[i] = temp[i + 1];
                        temp[i + 1] = temp2;
                    }
                }
            }
        }

        int topCount = 5;

        if (_localPlayerList.Count < 5)
        {
            topCount = _localPlayerList.Count;
        }

        for (int i = 0; i < topCount; i++)
        {
            if (topCount >= 5)
            {
                TopPlayers[i].SetActive(true);
                TopPlayers[i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[i]].Name;
                TopPlayers[i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[i]].CardsCompleted.ToString();

                BottomPlayers[4 - i].transform.GetChild(0).GetComponent<Text>().text = (_count - i).ToString();
                BottomPlayers[4 - i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].Name;
                BottomPlayers[4 - i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].CardsCompleted.ToString();


                BottomPlayers[4 - i].SetActive(true);
            }
            else
            {
                TopPlayers[i].SetActive(true);
                TopPlayers[i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[i]].Name;
                TopPlayers[i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[i]].CardsCompleted.ToString();

                BottomPlayers[topCount - 1 - i].transform.GetChild(0).GetComponent<Text>().text = (_count - i).ToString();
                BottomPlayers[topCount - 1 - i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].Name;
                BottomPlayers[topCount - 1 - i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].CardsCompleted.ToString();


                BottomPlayers[topCount - 1 - i].SetActive(true);
            }


            if (i == 0)
            {
                _cardHighNumber = _localPlayerList[temp[i]].CardsCompleted;
                _cardLowNumber = _localPlayerList[temp[(_count - 1) - i]].CardsCompleted;
            }

        }

        _cardTotalNumber = 0;

        foreach (var item in _localPlayerList)
        {
            _cardTotalNumber += item.CardsCompleted;
        }

        _cardAvgNumber = _cardTotalNumber / _count;

        _cardTotalText.text = _cardTotalNumber.ToString();
        _cardHighText.text = "HIGH: " + _cardHighNumber.ToString();
        _cardAvgText.text = "AVG: " + _cardAvgNumber.ToString();
        _cardlowText.text = "LOW: " + _cardLowNumber.ToString();

        if (_startGame == false)
        {
            _endCardTotalText.text = _cardTotalNumber + " total cards";
            _endCardHighAvgLowText.text = "HIGH: " + _cardHighNumber + "     AVG: " + _cardAvgNumber + "     LOW: " + _cardLowNumber;

            int finishTopCount = 3;

            if (_localPlayerList.Count < 3)
            {
                finishTopCount = _localPlayerList.Count;
            }

            for (int i = 0; i < finishTopCount; i++)
            {
                if (finishTopCount >= 3)
                {
                    Top3Players[i].SetActive(true);
                    Top3Players[i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[i]].Name;
                    Top3Players[i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[i]].CardsCompleted.ToString();

                    Bottom3Players[2 - i].transform.GetChild(0).GetComponent<Text>().text = (_count - i).ToString();
                    Bottom3Players[2 - i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].Name;
                    Bottom3Players[2 - i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].CardsCompleted.ToString();
                }
                else
                {
                    Top3Players[i].SetActive(true);
                    Top3Players[i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[i]].Name;
                    Top3Players[i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[i]].CardsCompleted.ToString();

                    Bottom3Players[finishTopCount - 1 - i].transform.GetChild(0).GetComponent<Text>().text = (_count - i).ToString();
                    Bottom3Players[finishTopCount - 1 - i].transform.GetChild(1).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].Name;
                    Bottom3Players[finishTopCount - 1 - i].transform.GetChild(2).GetComponent<Text>().text = _localPlayerList[temp[(_count - 1) - i]].CardsCompleted.ToString();

                    for (int j = 2; j >= 0 + finishTopCount; j--)
                    {
                        Top3Players[j].SetActive(false);
                        Bottom3Players[j].SetActive(false);
                    }
                }
            }
        }
        _loadingPanel.SetActive(false);
    }

    void StarChanged(int Star)
    {
        if (Star == 1)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _inactiveStar;
            _star3.image.sprite = _inactiveStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 2)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _inactiveStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 3)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 4)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _activeStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 5)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _activeStar;
            _star5.image.sprite = _activeStar;
        }

        _starRating = Star;
    }

    void EndGame()
    {
        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, EndingGame);
    }

    void EndingGame(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    item.StartGame = false;
                    item.InGame = false;

                    _table.Update<MeshworkingTable>(item);
                }
            }
        }
    }

    private void GetPlayerList()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);
        CustomQuery query = new CustomQuery(filter, "eventid desc", 50, _skip/*, "id,username,score"*/); //CustomQuery.OrderBy ("score desc");
        _table.Query<NestedResults<MeshworkingTable>>(query, OnReadNestedResultsCompleted); //Query(query);
    }

    private void OnReadNestedResultsCompleted(IRestResponse<NestedResults<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Debug.Log("OnReadNestedResultsCompleted: " + response.ResponseUri + " data: " + response.Content);
            List<MeshworkingTable> items = response.Data.results;
            _totalCount = response.Data.count;
            Debug.Log("Read items count: " + items.Count + "/" + response.Data.count);
            _isPaginated = true; // nested query will support pagination
            if (_skip != 0)
            {
                _localPlayerList.AddRange(items); // append results for paginated results
            }
            else
            {
                _localPlayerList = items; // set for first page of results
            }
            HasNewData = true;

            foreach (var item in _localPlayerList)
            {
                if (item.UniqueId == _uniqueId)
                {
                    _localPlayerList.Remove(item);
                    break;
                }
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }
        
        if (_localPlayerList.Count != _totalCount - 1)
        {
            _skip+=50;
            GetPlayerList();
        }
        else
        {
            _isLoadingNextPage = false; // allows next page to be loaded
            _populatePlayerList = true;
            _skip = 0;
        }
    }

    private void DeleteEvent()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);
        CustomQuery query = new CustomQuery(filter, "eventid desc", 0, _skip/*, "id,username,score"*/); //CustomQuery.OrderBy ("score desc");
        _table.Query<NestedResults<MeshworkingTable>>(query, DeletingEvents); //Query(query);
    }

    private void DeletingEvents(IRestResponse<NestedResults<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Debug.Log("OnReadNestedResultsCompleted: " + response.ResponseUri + " data: " + response.Content);
            List<MeshworkingTable> items = response.Data.results;
            _totalCount = response.Data.count;
            Debug.Log("Read items count: " + items.Count + "/" + response.Data.count);
            _isPaginated = true; // nested query will support pagination
            HasNewData = true;

            foreach (var item in response.Data.results)
            {
                _table.Delete<MeshworkingTable>(item.id);
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }

        if (_totalCount != 0)
        {
            DeleteEvent();
        }
        else
        {
            _isLoadingNextPage = false; // allows next page to be loaded
            _deletedEvent = true;
        }

    }
}
