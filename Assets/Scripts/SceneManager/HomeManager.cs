﻿using UnityEngine;
using System;
using System.Net;
using System.Collections.Generic;
using RestSharp;
using Pathfinding.Serialization.JsonFx;
using Unity3dAzure.AppServices;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour 
{
    private GameObject GameInformationObject;
    [SerializeField] private GameObject _middleLine;
    [SerializeField] private GameObject _createEventHolderGameobject;
    [SerializeField] private GameObject _inputHolderGameobject;
    [SerializeField] private GameObject _errorEventPanel;
    [SerializeField] private GameObject _helpVideoPanel;
    [SerializeField] private GameObject _createdEventPanel;

    [SerializeField] private Button _joinEventButton; 
    [SerializeField] private Button _createEventButton;
    [SerializeField] private Button _hostEventButton;
    [SerializeField] private Button _closePopUpButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _helpButton;
    [SerializeField] private Button _closeHelpButton;
    [SerializeField] private Button _helpVideoButton;
    [SerializeField] private Button _preVideoYesButton;
    [SerializeField] private Button _helpVideoYesButton;

    [SerializeField] private Text _orText;
    [SerializeField] private Text _eventText;

    [SerializeField] private InputField _eventCodeInput;
    
    
    [Space (5)]
    [Header ("Delete Event")]
    [Space(5)]
    [SerializeField] private GameObject _deleteAreYouSure;
    [SerializeField] private GameObject _deletingPanel;
    [SerializeField] private Button _deleteButton;
    [SerializeField] private Button _noDeleteButton;
    [SerializeField] private Button _yesDeleteButton;

    private string _uniqueId;
    private string _eventId;
    private string _eventName;

    private bool _createdEvent = false;
    private bool _isHost = false;
    private bool _joinRoom = false;
    private bool _deletedEvent = false;
    private bool _errorPopUp = false;

    private MobileServiceTable<MeshworkingTable> _table;

    private uint _skip = 0; // no of records to skip
    private uint _totalCount = 0; // count value should be > 0 to paginate

    private bool _isPaginated = false; // only enable infinite scrolling for paginated results
    private bool HasNewData = false; // to reload table view when data has changed
    private bool _isLoadingNextPage = false; // load once when scroll buffer is hit

	// Use this for initialization
	void Start () 
    {
        GameInformationObject = GameObject.Find("GameInformation");

        _eventId = GameInformationObject.GetComponent<GlobalInformation>().EventId;

        _table = GameInformationObject.GetComponent<AppServiceManager>().Table;

        _joinEventButton.onClick.AddListener(JoinEvent);

        _createEventButton.onClick.AddListener(() => GetComponent<CanvasManager>().OnHostProfileButtonClicked());

        _hostEventButton.onClick.AddListener(() => 
        {
            GetComponent<CanvasManager>().OnMeshworkingHostButtonClicked();
        });

        _closePopUpButton.onClick.AddListener(() => _errorEventPanel.SetActive(false));

        if (GameInformationObject.GetComponent<GlobalInformation>().IsHost)
        {
            _createdEventPanel.SetActive(true);
            _eventText.text = GameInformationObject.GetComponent<GlobalInformation>().EventId;

            _orText.gameObject.SetActive(false);
            _createEventHolderGameobject.SetActive(false);
            _inputHolderGameobject.SetActive(false);
            _middleLine.SetActive(true);
            //_deleteHolderGameobject.SetActive(true);

            _createEventButton.interactable = true;
        }

        GameInformationObject.GetComponent<GlobalInformation>().CameFromGame = false;

        _exitButton.onClick.AddListener(() => Application.Quit());

        _helpButton.onClick.AddListener(() => _helpVideoPanel.SetActive(true));

        _closeHelpButton.onClick.AddListener(() => _helpVideoPanel.SetActive(false));

        
        _preVideoYesButton.onClick.AddListener(() => 
        {
            Application.OpenURL("http://www.mobilemeshevents.com/explainer");
        });
        _helpVideoButton.onClick.AddListener(() => Application.OpenURL("http://www.mobilemeshevents.com/htp"));
        _helpVideoYesButton.onClick.AddListener(() => Application.OpenURL("http://www.mobilemeshevents.com/htp"));


        _deleteButton.onClick.AddListener(() => _deleteAreYouSure.SetActive(true));

        _yesDeleteButton.onClick.AddListener(() =>
        {
            _deletingPanel.SetActive(true);
            DeleteEvent();
        });

        _noDeleteButton.onClick.AddListener(() => _deleteAreYouSure.SetActive(false));

        /*
        for (int i = 0; i < 100; i++)
        {
            MeshworkingTable player2 = new MeshworkingTable();
            player2.Name = "Player " + i;
            player2.PhotosTaken = 6;
            player2.EventId = "B2VD";
            player2.UniqueId = Guid.NewGuid().ToString();
            player2.CardsCompleted = UnityEngine.Random.Range(0, 1000);

            _table.Insert<MeshworkingTable>(player2);
        }*/
	}

    void Update()
    {/*
        if (_createdEvent)
        {
            _createdEvent = false;

            _createdEventPanel.SetActive(true);
            _eventText.text = GameInformationObject.GetComponent<GlobalInformation>().EventId;

            _orText.gameObject.SetActive(false);
            _createEventHolderGameobject.SetActive(false);
            _inputHolderGameobject.SetActive(false);
            _middleLine.SetActive(true); 
            //_deleteHolderGameobject.SetActive(true);

            _createEventButton.interactable = true;
        }*/

        if (_joinRoom)
        {
            _joinRoom = false;
            if (_isHost == false)
            {
                GameInformationObject.GetComponent<GlobalInformation>().EventId = _eventCodeInput.text.ToUpper();
                GameInformationObject.GetComponent<GlobalInformation>().EventName = _eventName;

                if (GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName != null && GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName != " " && GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName != "")
                {
                    GetComponent<CanvasManager>().OnSocialScavengerHomeButtonClicked();
                }
                else
                {
                    GetComponent<CanvasManager>().OnProfileButtonClicked();
                }
            }
        }

        if (_deletedEvent)
        {
            _deletedEvent = false;

            _deleteAreYouSure.SetActive(false);
            _middleLine.SetActive(false);
            _inputHolderGameobject.SetActive(true);
            _orText.gameObject.SetActive(true);
            _createEventHolderGameobject.SetActive(true);

            GameInformationObject.GetComponent<GlobalInformation>().IsHost = false;
            GameInformationObject.GetComponent<GlobalInformation>().EventId = "";
            GameInformationObject.GetComponent<GlobalInformation>().EventName = "";

            PlayerPrefs.SetString("IsHost", "false");
            PlayerPrefs.SetString("roomcode", "");
            PlayerPrefs.SetString("EventName", "");

            _deletingPanel.SetActive(false);
            _createdEventPanel.SetActive(false);
        }

        if (_errorPopUp)
        {
            _errorPopUp = false;
            _errorEventPanel.SetActive(true);
            _joinEventButton.interactable = true;
        }
    }

    void JoinEvent()
    {
        _joinEventButton.interactable = false;

        string filter = string.Format("eventid eq '{0}'", _eventCodeInput.text);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, JoiningEvents); //Query(query);
    }

    void JoiningEvents(IRestResponse<List<MeshworkingTable>> response)
    {

        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.IsHost)
                {
                    _eventName = item.EventName;
                    _joinRoom = true;
                }
            }

            if (_joinRoom == false)
            {
                _errorPopUp = true;
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }
    }

    private void DeleteEvent()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);
        CustomQuery query = new CustomQuery(filter, "eventid desc", 0, _skip/*, "id,username,score"*/); //CustomQuery.OrderBy ("score desc");
        _table.Query<NestedResults<MeshworkingTable>>(query, OnReadNestedResultsCompleted); //Query(query);
    }

    private void OnReadNestedResultsCompleted(IRestResponse<NestedResults<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Debug.Log("OnReadNestedResultsCompleted: " + response.ResponseUri + " data: " + response.Content);
            List<MeshworkingTable> items = response.Data.results;
            _totalCount = response.Data.count;
            Debug.Log("Read items count: " + items.Count + "/" + response.Data.count);
            _isPaginated = true; // nested query will support pagination
            HasNewData = true;

            foreach (var item in response.Data.results)
            {
                _table.Delete<MeshworkingTable>(item.id);
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }

        if (_totalCount != 0)
        {
            DeleteEvent();
        }
        else
        {
            _isLoadingNextPage = false; // allows next page to be loaded
            _deletedEvent = true;
        }

    }
}
