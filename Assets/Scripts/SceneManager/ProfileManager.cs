﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileManager : MonoBehaviour 
{
    private GameObject GameInformationObject;

    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _backButton;

    [SerializeField] private InputField _firstNameInput;
    [SerializeField] private InputField _lastNameInput;
    [SerializeField] private InputField _emailInput;

    private string _firstName;
    private string _lastName;
    private string _email;

    void Start()
    {

        GameInformationObject = GameObject.Find("GameInformation");

        _firstName = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName;
        _lastName = GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;
        _email = GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail;

        if (_firstName != null && _firstName != "")
        {
            _firstNameInput.text = _firstName;
        }

        if (_lastName != null && _lastName != "")
        {
            _lastNameInput.text = _lastName;
        }

        if (_email != null && _email != "")
        {
            _emailInput.text = _email;
        }

        if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "")
        {
            _nextButton.interactable = true;
        }
        else
        {
            _nextButton.interactable = false;
        }

        _firstNameInput.onValueChanged.AddListener(delegate
        {
            _firstName = _firstNameInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "")
            {
                _nextButton.interactable = true;
            }
            else
            {
                _nextButton.interactable = false;
            }
        });

        _lastNameInput.onValueChanged.AddListener(delegate 
        { 
            _lastName = _lastNameInput.text;
            if (_firstName != null && _firstName != "" && _lastName != null && _lastName != "")
            {
                _nextButton.interactable = true;
            }
            else
            {
                _nextButton.interactable = false;
            }
        });

        _nextButton.onClick.AddListener(() =>
        {
            GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName = _firstName;
            GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName = _lastName;

            GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail = _emailInput.text;

            PlayerPrefs.SetString("FirstName", _firstName);
            PlayerPrefs.SetString("LastName", _lastName);
            PlayerPrefs.SetString("Email", _emailInput.text);
            
            GetComponent<CanvasManager>().OnSocialScavengerHomeButtonClicked();
        });

        _backButton.onClick.AddListener(() =>
        {
            if (GameInformationObject.GetComponent<GlobalInformation>().CameFromGame)
            {
                GetComponent<CanvasManager>().OnSocialScavengerHomeButtonClicked();
            }
            else
            {
                GetComponent<CanvasManager>().OnHomeButtonClicked();
            }
        });
    }
}
