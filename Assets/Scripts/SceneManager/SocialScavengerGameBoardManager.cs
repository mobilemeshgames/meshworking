using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity3dAzure.AppServices;
using RestSharp;
using Pathfinding.Serialization.JsonFx;

public class SocialScavengerGameBoardManager : MonoBehaviour {

	GameObject GameInformationObject;
    [SerializeField] private GameObject _completedPanel;
    [SerializeField] private GameObject _centerCardGameobject;
    [SerializeField] private GameObject _cameraPanel;
    [SerializeField] private GameObject _cardHolder;
	[SerializeField] private GameObject _searchBox;
    [SerializeField] private GameObject _pleaseRatePanel;
    [SerializeField] private GameObject _loadingPanel;
	
    [SerializeField] private Button _leftButton;
    [SerializeField] private Button _rightButton;
    [SerializeField] private Button _completeButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _star1;
    [SerializeField] private Button _star2;
    [SerializeField] private Button _star3;
    [SerializeField] private Button _star4;
    [SerializeField] private Button _star5;
    [SerializeField] private Button _closePleaseRateButton;
    [SerializeField] private Button _backToCardsButton;
    [SerializeField] private Button _editButton;
    [SerializeField] private Button _cameraButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _closePhotoButton;
    [SerializeField] private Button _takePhotoButton;
    [SerializeField] private Button _customNameButton;
	
    [SerializeField] private Image _fakeLeftCardImage;
    [SerializeField] private Image _farLeftCardImage;
	[SerializeField] private Image _leftCardImage;
    [SerializeField] private Image _centerCardImage;
    [SerializeField] private Image _rightCardImage;
    [SerializeField] private Image _farRightCardImage;
    [SerializeField] private Image _card6Image;
    [SerializeField] private Image _fakeRightCardImage;
	[SerializeField] private Image _Card1Circle;
	[SerializeField] private Image _Card2Circle;
	[SerializeField] private Image _Card3Circle;
	[SerializeField] private Image _Card4Circle;
	[SerializeField] private Image _Card5Circle;
	[SerializeField] private Image _Card6Circle;

    [SerializeField] private RawImage _cameraScreen;

    [SerializeField] private Text _fakeLeftCardText;
    [SerializeField] private Text _farLeftCardText;
    [SerializeField] private Text _leftCardText;
    [SerializeField] private Text _centerCardText;
    [SerializeField] private Text _rightCardText;
    [SerializeField] private Text _farRightCardText;
    [SerializeField] private Text _card6Text;
    [SerializeField] private Text _fakeRightCardText;
    [SerializeField] private Text _fakeLeftCardAnswerText;
    [SerializeField] private Text _farLeftCardAnswerText;
    [SerializeField] private Text _leftCardAnswerText;
    [SerializeField] private Text _rightCardAnswerText;
    [SerializeField] private Text _farRightCardAnswerText;
    [SerializeField] private Text _card6AnswerText;
    [SerializeField] private Text _fakeRightCardAnswerText;
    [SerializeField] private Text _completedCountText;
    [SerializeField] private Text _centerNormalNameText;

    [SerializeField] private InputField _fakeLeftCardInput;
    [SerializeField] private InputField _farLeftCardInput;
    [SerializeField] private InputField _leftCardInput;
    [SerializeField] private InputField _centerCardInput;
    [SerializeField] private InputField _rightCardInput;
    [SerializeField] private InputField _farRightCardInput;
    [SerializeField] private InputField _card6Input;
    [SerializeField] private InputField _fakeRightCardInput;

    [SerializeField] private Sprite _activeStar;
    [SerializeField] private Sprite _inactiveStar;
    [SerializeField] private Sprite _normalPip;
    [SerializeField] private Sprite _cardBackground;

    private int _questionCounter = 0;
	private int _r = 1;
	private int _g = 1;
	private int _b = 1;
    private int _answerCounter = 0;
    private int _photoCounter = 0;
    private int _previousPhotoCounter = 0;
    private int _totalTime = 0;
    private int _previousTotalTime = 0;
    private int _roundedRestSeconds;
    private int _yourPosition = 0;
    private int _starRating;
    private int _slideSpeed = 15;
    private int _cardNum = 3;

    private int _cardsCompleted = 0;
    private int _previousCardsCompleted = 0;
    private int _picturesTaken = 0;

    private GameObject _qrPanel = null;

    public List<string> Questions = new List<string>();
    public List<string> Answer = new List<string>();
    private List<int> _timeList = new List<int>();
    public List<Sprite> _pictureList = new List<Sprite>();
    public List<Texture2D> testlist = new List<Texture2D>();

    private List<MeshworkingTable> _playerList = new List<MeshworkingTable>();

    private float swipeValue;
    private float swipeDist;
    private float _startTime;
    private float _timer;
    private float _restSeconds;
    private float _displaySeconds;
    private float _displayMinutes;
    private float _xValue = 0;

    private Vector2 startPos;

    private bool _cantSwipe = false;
    private bool _alreadyPopped = false;
	private bool stopper = false;
    private bool _moveLeft = false;
    private bool _moveRight = false;
    private bool _checkedAzure = false;
    private bool _leftGame = false;
    private bool _removedHost = false;

    private string _uniqueId;
    private string _eventId;
    private string _name;

    private MeshworkingTable _player = new MeshworkingTable();

    private WebCamTexture _wct;

    private MobileServiceTable<MeshworkingTable> _table;
    private MobileServiceTable<Meshworking_Results_Participant> _statsTable;

    private uint _skip = 0; // no of records to skip
    private uint _totalCount = 0; // count value should be > 0 to paginate

    private bool _isPaginated = false; // only enable infinite scrolling for paginated results
    private bool HasNewData = false; // to reload table view when data has changed
    private bool _isLoadingNextPage = false; // load once when scroll buffer is hit

	void Start () 
	{
		GameInformationObject = GameObject.Find ("GameInformation");

        GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().Shuffle();

        _eventId = GameInformationObject.GetComponent<GlobalInformation>().EventId;
        _uniqueId = GameInformationObject.GetComponent<GlobalInformation>().UniqueId;
        _name = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName + " " + GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;

        _table = GameInformationObject.GetComponent<AppServiceManager>().Table;
        _statsTable = GameInformationObject.GetComponent<AppServiceManager>().StatsTable;

        _startTime = Time.time;

        for (int i = 0; i < GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().preFoundStatements.Count; i++)
        {
            Answer.Add("");
            _pictureList.Add(null);
        }

        _Card1Circle.color = new Color32(246,142,31,255);

        if (GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().GlobalAnswerList.Count != 0)
        {
            Answer = GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().GlobalAnswerList;
            _pictureList = GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().GlobalPictureList;
            FillInAnswers();
        }

        Questions = GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().preFoundStatements;
        
        _leftCardText.text = Questions[Questions.Count - 1];
        _centerCardText.text = Questions[_questionCounter];
        _rightCardText.text = Questions[_questionCounter + 1];
		
        _leftButton.onClick.AddListener(DecreaseCounter);
        _rightButton.onClick.AddListener(IncreaseCounter);

        _centerCardInput.onValueChanged.AddListener(delegate { CheckInfo(); });

        _customNameButton.onClick.AddListener(() => 
        {
            _searchBox.SetActive(false);
            AddName();
        });

        _completeButton.onClick.AddListener(() =>
        {
            _backButton.gameObject.SetActive(false);
            _completedPanel.SetActive(true);
            GetFinishedCount();
            StartCoroutine(DownloadImages());
        });

        _backButton.onClick.AddListener(() =>
        {
            GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().CameFromGame = true;
            GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().GlobalAnswerList = Answer;
            GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().GlobalPictureList = _pictureList;
        });

        _star1.onClick.AddListener(() => StarChanged(1));
        _star2.onClick.AddListener(() => StarChanged(2));
        _star3.onClick.AddListener(() => StarChanged(3));
        _star4.onClick.AddListener(() => StarChanged(4));
        _star5.onClick.AddListener(() => StarChanged(5));

        _backToCardsButton.onClick.AddListener(() => 
        {
            _completedPanel.SetActive(false);
            _backButton.gameObject.SetActive(true);
        });

        _editButton.onClick.AddListener(() =>
        {
            _centerCardInput.interactable = true;
            _editButton.gameObject.SetActive(false);
            _cameraButton.gameObject.SetActive(true);
            _centerCardInput.gameObject.SetActive(true);
        });

        _exitButton.onClick.AddListener(() =>
        {
            if (_starRating > 0)
            {
                GameInformationObject.GetComponent<GlobalRating>().RateGame(_starRating, 2);
                LeaveGame();
                _exitButton.interactable = false;
            }
            else
            {
                _pleaseRatePanel.SetActive(true);
            }
        });

        _closePleaseRateButton.onClick.AddListener(() => _pleaseRatePanel.SetActive(false));

        _cameraButton.onClick.AddListener(() => 
        {
            _cameraPanel.SetActive(true);
            _cameraButton.gameObject.SetActive(false);
            _wct.Play();
        });

        _closePhotoButton.onClick.AddListener(() =>
        {
            _cameraPanel.SetActive(false);
            _wct.Stop();
        });

        _takePhotoButton.onClick.AddListener(() => StartCoroutine(TakePicture()));
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length > 0)
        {
#if !UNITY_EDITOR && !UNITY_STANDALONE && !UNITY_IOS
            //float _width = _cameraScreen.GetComponent<RectTransform>().rect.height;
            //float _height = _cameraScreen.GetComponent<RectTransform>().rect.width;
            //_cameraScreen.GetComponent<RectTransform>().sizeDelta = new Vector2(_width, _height);

            //_cameraScreen.transform.localEulerAngles = new Vector3(0, 0, 90);
#elif UNITY_IOS
			_cameraScreen.transform.localEulerAngles = new Vector3(0, 180, -90);
#endif
            string deviceName;
            deviceName = devices[0].name;

            _wct = new WebCamTexture(deviceName, (int)_cameraScreen.rectTransform.sizeDelta.x, (int)_cameraScreen.rectTransform.sizeDelta.y, 20);
            _cameraScreen.material.mainTexture = _wct;
        }

        foreach (var item in _playerList)
        {
            _searchBox.GetComponent<NameSearch>().allNames.Add(item.Name);
            _searchBox.GetComponent<NameSearch>().UpdateList();
        }

        StartGetNames();
        
	}
	
    void Update()
    {
        if (_completedPanel.activeSelf == false)
        {
            if (Input.touchCount > .75f) // Size of swipe is big enough	
            {
                Touch touch = Input.touches[0];

                switch (touch.phase)
                {
                    case TouchPhase.Began: // Start of swipe
                        startPos = touch.position;
                        break;
                    case TouchPhase.Ended: // End of swipe
                        if (_cantSwipe == false)
                        {
                            swipeValue = Mathf.Sign(touch.position.x - startPos.x); // Returns 1 or -1 to determine if it was a left or right swipe
                            swipeDist = Mathf.Abs(touch.position.x - startPos.x); // Length of swipe 
                        }
                        else
                        {
                            _cantSwipe = false;
                        }
                        break;
                    default:
                        break;
                }
            }

            if (swipeValue > 0 && swipeDist > 125 && ((float)(Input.mousePosition.y / Screen.height) > .2f) && ((float)(Input.mousePosition.y / Screen.height) < .9f))
            {
                //right swipe
                DecreaseCounter();
            }
            else if (swipeValue < 0 && swipeDist > 125 && ((float)(Input.mousePosition.y / Screen.height) > .2f) && ((float)(Input.mousePosition.y / Screen.height) < .9))
            {
                //left swipe
                IncreaseCounter();
            }
        }
        else
        {
            swipeValue = 0;
            swipeDist = 0;
        }

        if (_moveLeft)
        {
            if (_cardHolder.transform.localPosition.x < _xValue)
            {
                if (_cardHolder.transform.localPosition.x > _xValue - 50)
                {
                    _slideSpeed = 2;
                }
                else if (_cardHolder.transform.localPosition.x > _xValue - 150)
                {
                    _slideSpeed = 5;
                }
                else
                {
                    _slideSpeed = 15;
                }

                _cardHolder.transform.Translate(Vector2.right * (Time.deltaTime) * _slideSpeed, Space.World);
                _editButton.gameObject.SetActive(false);
                _cameraButton.gameObject.SetActive(false);

                if (_cardNum < 0)
                {
                    _cardNum = 6;
                    _cardHolder.transform.localPosition = new Vector3(-1800, 0, 0);
                    AssignCards();
                }
            }
            else
            {
                _cardHolder.transform.localPosition = new Vector3(_xValue, 0, 0);

                _moveLeft = false;
                _moveRight = false;
                
                /*if (_cardNum < 0)
                {
                    _cardNum = 6;
                    _cardHolder.transform.localPosition = new Vector3(-1800, 0, 0);
                    AssignCards();
                }
                else */if (_cardNum != 3)
                {
                    _cardNum = 3;
                    _xValue = 360;
                    _cardHolder.transform.localPosition = new Vector3(_xValue, 0, 0);

                    AssignCards();
                }
            }
        }

        if (_moveRight)
        {
            if (_cardHolder.transform.localPosition.x > _xValue)
            {
                if (_cardHolder.transform.localPosition.x < _xValue + 50)
                {
                    _slideSpeed = 2;
                }
                else if (_cardHolder.transform.localPosition.x < _xValue + 150)
                {
                    _slideSpeed = 5;
                }
                else
                {
                    _slideSpeed = 15;
                }
                _cardHolder.transform.Translate(Vector2.right * (Time.deltaTime) * -_slideSpeed, Space.World);
                _editButton.gameObject.SetActive(false);
                _cameraButton.gameObject.SetActive(false);
                if (_cardNum > 7)
                {
                    _cardNum = 1;
                    _cardHolder.transform.localPosition = new Vector3(1800, 0, 0);
                    AssignCards();
                }
            }
            else
            {
                _cardHolder.transform.localPosition = new Vector3(_xValue, 0, 0);

                _moveLeft = false;
                _moveRight = false;
                /*
                if (_cardNum > 6)
                {
                    _cardNum = 1;
                    _cardHolder.transform.localPosition = new Vector3(1800, 0, 0);
                    AssignCards();
                }
                else */if (_cardNum != 3)
                {
                    _cardNum = 3;
                    _xValue = 360;
                    _cardHolder.transform.localPosition = new Vector3(_xValue, 0, 0);
                    AssignCards();
                }
            }
        }

        if (_checkedAzure)
        {
            _checkedAzure = false;

            _loadingPanel.SetActive(false);

            _searchBox.GetComponent<NameSearch>().allNames.Clear();

            /*List<string> tempList = new List<string>();

            foreach (var item in Answer)
            {
                tempList.Add(item);
            }*/

            foreach (var item in _playerList)
            {
                if (item.IsHost == false && item.UniqueId != _uniqueId)
                {
                    _searchBox.GetComponent<NameSearch>().allNames.Add(item.Name);
                }
                else if(item.UniqueId == _uniqueId && _player.Name == null)
                {
                    _player = item;
                }
            }
            _searchBox.GetComponent<NameSearch>().UpdateList();
        }

        if (_leftGame)
        {
            _leftGame = false;
            GetComponent<CanvasManager>().OnSurveyButtonClicked();
        }
    }

    void CheckInfo()
    {
        if (_centerCardInput.text == "")
        {
            _customNameButton.gameObject.SetActive(false);
        }
        else
        {
            _customNameButton.gameObject.SetActive(true);
        }
    }

    public void AddName()
    {
        Answer[_questionCounter] = _centerCardInput.text;
		if(_centerCardInput.text == null || _centerCardInput.text == "")
        {

		}
        else
        {
			switch (_questionCounter)
			{
				case 0:
				_Card1Circle.color = new Color32(168, 205, 57, 255);
				break;
				case 1:
				_Card2Circle.color = new Color32(168, 205, 57, 255);
				break;
				case 2:
				_Card3Circle.color = new Color32(168, 205, 57, 255);
				break;
				case 3:
				_Card4Circle.color = new Color32(168, 205, 57, 255);
				break;
				case 4:
				_Card5Circle.color = new Color32(168, 205, 57, 255);
				break;
				case 5:
				_Card6Circle.color = new Color32(168, 205, 57, 255);
				break;
				default:
				break;
			}

            if (_pictureList[_questionCounter] == null)
            {
                _centerCardImage.color = new Color32(168, 205, 57, 255);
            }

            _answerCounter = 0;

            foreach (var item in Answer)
            {
                if (item != null && item != "" && item != " ")
                {
                    _answerCounter++;
                }
            }

            _player.CardsCompleted = _answerCounter;

            _table.Update<MeshworkingTable>(_player);

            _cameraButton.gameObject.SetActive(true);
            
            _centerNormalNameText.text = _centerCardInput.text;
            _centerCardInput.gameObject.SetActive(false);
		}
    }

    void DecreaseCounter()
    {
        _searchBox.SetActive(false);
        _cardNum--;

        switch (_cardNum)
        {
            case 0:
                _xValue = 2520;
                break;
            case 1:
                _xValue = 1800;
                break;
            case 2:
                _xValue = 1080;
                break;
            case 3:
                _xValue = 360;
                break;
            case 4:
                _xValue = -360;
                break;
            case 5:
                _xValue = -1080;
                break;
            case 6:
                _xValue = -1800;
                break;
            case 7:
                _xValue = -2520;
                break;
            default:
                _cardNum = 6;
                break;
        }

        _moveLeft = true;
        _moveRight = false;
        swipeValue = 0;
        swipeDist = 0;

        _questionCounter--;
		
    }

    void IncreaseCounter()
    {
        _searchBox.SetActive(false);
        _cardNum++;

        switch (_cardNum)
        {
            case 0:
                _xValue = 2520;
                break;
            case 1:
                _xValue = 1800;
                break;
            case 2:
                _xValue = 1080;
                break;
            case 3:
                _xValue = 360;
                break;
            case 4:
                _xValue = -360;
                break;
            case 5:
                _xValue = -1080;
                break;
            case 6:
                _xValue = -1800;
                break;
            case 7:
                _xValue = -2520;
                break;
            default:
                _cardNum = 1;
                break;
        }

        _moveLeft = false;
        _moveRight = true;
        swipeValue = 0;
        swipeDist = 0;

        _questionCounter++;
    }

    private IEnumerator Screenshot()
    {

        //yield return new WaitForSeconds(1);
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        int widthvalue = (int)(Screen.width * .83f);
        int heightvalue = (int)(Screen.height * .645f);
        Texture2D tex = new Texture2D(widthvalue, heightvalue, TextureFormat.RGB24, false);
        // Read screen contents into the texture

        tex.ReadPixels(new Rect(((Screen.width / 2) - Screen.width * 0.41f), ((Screen.height / 2) - Screen.height * .3f), Screen.width * .85f, Screen.height), 0, 0);
        tex.Apply();

        UM_Camera.Instance.SaveImageToGalalry(tex);
        Destroy(tex);
    }

    void ResetColor()
    {
        int answerSegment = 0;
        int lowestSegment = 0;
        int highestSegment = 0;
        
        if(_questionCounter != 0)
        {
            answerSegment = _questionCounter / 6;
        }
        else
        {
            answerSegment = 0;
        }

        if(answerSegment != 0)
        {
            lowestSegment = 6 * answerSegment;
            highestSegment = 6 * (answerSegment + 1);
        }
        else
        {
            lowestSegment = 0;
            highestSegment = 6;
        }

        if (highestSegment > Questions.Count)
        {
            switch (highestSegment - Questions.Count)
            {
                case 1:
                    _Card6Circle.gameObject.SetActive(false);
                    break;
                case 2:
                    _Card5Circle.gameObject.SetActive(false);
                    _Card6Circle.gameObject.SetActive(false);
                    break;
                case 3:
                    _Card4Circle.gameObject.SetActive(false);
                    _Card5Circle.gameObject.SetActive(false);
                    _Card6Circle.gameObject.SetActive(false);
                    break;
                case 4:
                    _Card3Circle.gameObject.SetActive(false);
                    _Card4Circle.gameObject.SetActive(false);
                    _Card5Circle.gameObject.SetActive(false);
                    _Card6Circle.gameObject.SetActive(false);
                    break;
                case 5:
                    _Card2Circle.gameObject.SetActive(false);
                    _Card3Circle.gameObject.SetActive(false);
                    _Card4Circle.gameObject.SetActive(false);
                    _Card5Circle.gameObject.SetActive(false);
                    _Card6Circle.gameObject.SetActive(false);
                    break;
            }
            highestSegment = Questions.Count;
        }
        else
        {
            _Card1Circle.gameObject.SetActive(true);
            _Card2Circle.gameObject.SetActive(true);
            _Card3Circle.gameObject.SetActive(true);
            _Card4Circle.gameObject.SetActive(true);
            _Card5Circle.gameObject.SetActive(true);
            _Card6Circle.gameObject.SetActive(true);
        }

        _Card1Circle.sprite = _normalPip;
        _Card2Circle.sprite = _normalPip;
        _Card3Circle.sprite = _normalPip;
        _Card4Circle.sprite = _normalPip;
        _Card5Circle.sprite = _normalPip;
        _Card6Circle.sprite = _normalPip;

        for (int i = lowestSegment; i < highestSegment; i++)
        {
            if (Answer[i] == null || Answer[i] == "")
            {
                switch (i % 6)
                {
                    case 0:
                        _Card1Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    case 1:
                        _Card2Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    case 2:
                        _Card3Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    case 3:
                        _Card4Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    case 4:
                        _Card5Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    case 5:
                        _Card6Circle.color = new Color32(204, 204, 204, 255);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (i % 6)
                {
                    case 0:
                        _Card1Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 1:
                        _Card2Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 2:
                        _Card3Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 3:
                        _Card4Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 4:
                        _Card5Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 5:
                        _Card6Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    void FillInAnswers()
    {
        for(int i = 0; i < 6; i++)
        {
            if (Answer[i] != null && Answer[i] != "")
            {
                switch (i)
                {
                    case 0:
                        _Card1Circle.color = new Color32(246, 142, 31, 255);
                        if (_pictureList[0] != null)
                        {
                            _centerCardImage.sprite = _pictureList[0];
                        }
                        else
                        {
                            _centerCardImage.color = new Color32(168, 205, 57, 255);
                        }

                        _centerCardInput.text = Answer[0];
                        _centerCardInput.interactable = false;
                        _editButton.gameObject.SetActive(true);
                        _cameraButton.gameObject.SetActive(false);
                        _centerNormalNameText.text = _centerCardInput.text;
                        _centerCardInput.gameObject.SetActive(false);

                        break;
                    case 1:
                        _Card2Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 2:
                        _Card3Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 3:
                        _Card4Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 4:
                        _Card5Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    case 5:
                        _Card6Circle.color = new Color32(168, 205, 57, 255);
                        break;
                    default:
                        break;
                }
            }
        }
        
    }

    void StarChanged(int Star)
    {
        if (Star == 1)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _inactiveStar;
            _star3.image.sprite = _inactiveStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 2)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _inactiveStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 3)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _inactiveStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 4)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _activeStar;
            _star5.image.sprite = _inactiveStar;
        }
        else if (Star == 5)
        {
            _star1.image.sprite = _activeStar;
            _star2.image.sprite = _activeStar;
            _star3.image.sprite = _activeStar;
            _star4.image.sprite = _activeStar;
            _star5.image.sprite = _activeStar;
        }

        _starRating = Star;
    }

    void GetFinishedCount()
    {
        int count = 0;
        for (int i = 0; i < Answer.Count; i++)
        {
            if (Answer[i] != null && Answer[i] != "")
            {
                count++;
            }
        }

        _completedCountText.text = count + "/" + Answer.Count;

        _cardsCompleted = count;

        _totalTime = (int)(Time.time - _startTime);
        print(_totalTime);

        SendResults();
    }

    IEnumerator TakePicture()
    {
        _closePhotoButton.gameObject.SetActive(false);
        _takePhotoButton.gameObject.SetActive(false);
        _wct.Stop();
        yield return new WaitForEndOfFrame();


        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        //UM_Camera.Instance.SaveImageToGalalry(tex);

        Resources.UnloadUnusedAssets();

        _centerCardImage.sprite = Sprite.Create(tex, new Rect(0, 0, width, height), Vector2.zero);
        _centerCardImage.color = new Color32(204, 204, 204, 255);

        _pictureList[_questionCounter] = _centerCardImage.sprite;
        testlist.Add(tex);

        _cameraPanel.SetActive(false);
        _closePhotoButton.gameObject.SetActive(true);
        _takePhotoButton.gameObject.SetActive(true);

        _photoCounter = 0;

        foreach (var item in _pictureList)
        {
            if (item != null)
            {
                _photoCounter++;
            }
        }

        //_player.Photostaken = _photoCounter;

        _table.Update<MeshworkingTable>(_player);

    }

    void AssignCards()
    {
        int FakeLeftModifier;
        int FarLeftModifier;
        int LeftModifier;
        int RightModifier;
        int FarRightModifier;
        int Card6Modifier;
        int FakeRightCardModifier;

        //Reset _questionCounter if needed
        if (_questionCounter < 0)
        {
            _questionCounter = Questions.Count - 1;
        }
        else if (_questionCounter >= Questions.Count)
        {
            _questionCounter = 0;
        }

        //Modifiers to help reduce code for the other cards
        if (_questionCounter == 0)
        {
            FakeLeftModifier = Questions.Count - 3;
            FarLeftModifier = Questions.Count - 2;
            LeftModifier = Questions.Count - 1;
            RightModifier = 1;
            FarRightModifier = 2;
            Card6Modifier = 3;
            FakeRightCardModifier = 4;
        }
        else if (_questionCounter == 1)
        {
            FakeLeftModifier = Questions.Count - 2;
            FarLeftModifier = Questions.Count - 1;
            LeftModifier = 1;
            RightModifier = 1;
            FarRightModifier = 2;
            Card6Modifier = 3;
            FakeRightCardModifier = 4;
        }
        else if (_questionCounter == Questions.Count - 2)
        {
            FakeLeftModifier = 3;
            FarLeftModifier = 2;
            LeftModifier = 1;
            RightModifier = 1;
            FarRightModifier = -_questionCounter;
            Card6Modifier = -(Questions.Count - 2);
            FakeRightCardModifier = -(Questions.Count - 3);
        }
        else if (_questionCounter == Questions.Count - 1)
        {
            FakeLeftModifier = 3;
            FarLeftModifier = 2;
            LeftModifier = 1;
            RightModifier = -_questionCounter;
            FarRightModifier = -(Questions.Count - 2);
            Card6Modifier = -(Questions.Count - 3);
            FakeRightCardModifier = -(Questions.Count - 4);
        }
        else
        {
            FakeLeftModifier = 3;
            FarLeftModifier = 2;
            LeftModifier = 1;
            RightModifier = 1;
            FarRightModifier = 2;
            Card6Modifier = 3;
            FakeRightCardModifier = 4;
        }
        if (_questionCounter - FakeLeftModifier > 0)
        {
        _fakeLeftCardText.text = Questions[Mathf.Abs(_questionCounter - FakeLeftModifier)];
        }
        if (_questionCounter - FarLeftModifier > 0)
        {
        _farLeftCardText.text = Questions[Mathf.Abs(_questionCounter - FarLeftModifier)];
        }
        if (_questionCounter - LeftModifier > 0)
        {
            _leftCardText.text = Questions[Mathf.Abs(_questionCounter - LeftModifier)];
        }
        _centerCardText.text = Questions[_questionCounter];
        if (_questionCounter + RightModifier < Questions.Count)
        {
            _rightCardText.text = Questions[_questionCounter + RightModifier];
        }
        if (_questionCounter + FarRightModifier < Questions.Count)
        {
            _farRightCardText.text = Questions[_questionCounter + FarRightModifier];
        }
        if (_questionCounter + Card6Modifier < Questions.Count)
        {
            _card6Text.text = Questions[_questionCounter + Card6Modifier];
        }
        if (_questionCounter + FakeRightCardModifier < Questions.Count)
        {
            _fakeRightCardText.text = Questions[_questionCounter + FakeRightCardModifier];
        }

        if (_questionCounter - FakeLeftModifier > 0)
        {
            _fakeLeftCardAnswerText.text = Answer[Mathf.Abs(_questionCounter - FakeLeftModifier)];
        }
        if (_questionCounter - FarLeftModifier > 0)
        {
            _farLeftCardAnswerText.text = Answer[Mathf.Abs(_questionCounter - FarLeftModifier)];
        }
        if (_questionCounter - LeftModifier > 0)
        {
            _leftCardAnswerText.text = Answer[Mathf.Abs(_questionCounter - LeftModifier)];
        }
        if (_questionCounter + RightModifier < Answer.Count)
        {
            _rightCardAnswerText.text = Answer[_questionCounter + RightModifier];
        }
        if (_questionCounter + FarRightModifier < Answer.Count)
        {
            _farRightCardAnswerText.text = Answer[_questionCounter + FarRightModifier];
        }
        if (_questionCounter + Card6Modifier < Answer.Count)
        {
            _card6AnswerText.text = Answer[_questionCounter + Card6Modifier];
        }
        if (_questionCounter + FakeRightCardModifier < Answer.Count)
        {
            _fakeRightCardAnswerText.text = Answer[_questionCounter + FakeRightCardModifier];
        }

        //Fake Left Card
        if ((_questionCounter + FakeLeftModifier > 0) && Answer[Mathf.Abs(_questionCounter - FakeLeftModifier)] != null && Answer[Mathf.Abs(_questionCounter - FakeLeftModifier)] != "" && Answer[Mathf.Abs(_questionCounter - FakeLeftModifier)] != " ")
        {
            if (_pictureList[Mathf.Abs(_questionCounter - FakeLeftModifier)] == null)
            {
                _fakeLeftCardImage.color = new Color32(168, 205, 57, 255);
                _fakeLeftCardImage.sprite = null;
            }
            else
            {
                _fakeLeftCardImage.sprite = _pictureList[Mathf.Abs(_questionCounter - FakeLeftModifier)];
                _fakeLeftCardImage.color = new Color32(204, 204, 204, 255);
            }
            _fakeLeftCardInput.gameObject.SetActive(false);
        }
        else
        {
            _fakeLeftCardImage.sprite = _cardBackground;
            _fakeLeftCardImage.color = new Color32(204, 204, 204, 255);
            _fakeLeftCardInput.gameObject.SetActive(true);
        }

        //Far Left Card
        if ((_questionCounter + FarLeftModifier > 0) && Answer[Mathf.Abs(_questionCounter - FarLeftModifier)] != null && Answer[Mathf.Abs(_questionCounter - FarLeftModifier)] != "" && Answer[Mathf.Abs(_questionCounter - FarLeftModifier)] != " ")
        {
            if (_pictureList[Mathf.Abs(_questionCounter - FarLeftModifier)] == null)
            {
                _farLeftCardImage.color = new Color32(168, 205, 57, 255);
                _farLeftCardImage.sprite = null;
            }
            else
            {
                _farLeftCardImage.sprite = _pictureList[Mathf.Abs(_questionCounter - FarLeftModifier)];
                _farLeftCardImage.color = new Color32(204, 204, 204, 255);
            }
            _farLeftCardInput.gameObject.SetActive(false);
        }
        else
        {
            _farLeftCardImage.sprite = _cardBackground;
            _farLeftCardImage.color = new Color32(204, 204, 204, 255);
            _farLeftCardInput.gameObject.SetActive(true);
        }

        //Left Card
        if ((_questionCounter + LeftModifier > 0) && Answer[Mathf.Abs(_questionCounter - LeftModifier)] != null && Answer[Mathf.Abs(_questionCounter - LeftModifier)] != "" && Answer[Mathf.Abs(_questionCounter - LeftModifier)] != " ")
        {
            if (_pictureList[Mathf.Abs(_questionCounter - LeftModifier)] == null)
            {
                _leftCardImage.color = new Color32(168, 205, 57, 255);
                _leftCardImage.sprite = null;
            }
            else
            {
                _leftCardImage.sprite = _pictureList[Mathf.Abs(_questionCounter - LeftModifier)];
                _leftCardImage.color = new Color32(204, 204, 204, 255);
            }
            _leftCardInput.gameObject.SetActive(false);
        }
        else
        {
            _leftCardImage.sprite = _cardBackground;
            _leftCardImage.color = new Color32(204, 204, 204, 255);
            _leftCardInput.gameObject.SetActive(true);
        }

        //Center Card
        if (Answer[_questionCounter] != null && Answer[_questionCounter] != "" && Answer[_questionCounter] != " ")
        {
            if (_pictureList[_questionCounter] == null)
            {
                _centerCardImage.color = new Color32(168, 205, 57, 255);
                _centerCardImage.sprite = null;
            }
            else
            {
                _centerCardImage.sprite = _pictureList[_questionCounter];
                _centerCardImage.color = new Color32(204, 204, 204, 255);
            }
            _centerCardInput.text = Answer[_questionCounter];
            _centerNormalNameText.text = _centerCardInput.text;
            _centerCardInput.gameObject.SetActive(false);
            _centerCardInput.interactable = false;
            _editButton.gameObject.SetActive(true);
            _cameraButton.gameObject.SetActive(false);
        }
        else
        {
            _centerCardImage.sprite = _cardBackground;
            _centerCardImage.color = new Color32(204, 204, 204, 255);
            
            _centerCardInput.text = "";
            _centerNormalNameText.text = "";
            _centerCardInput.gameObject.SetActive(true);
            _centerCardInput.interactable = true;
            _editButton.gameObject.SetActive(false);
        }

        //Right Card
        if ((_questionCounter + RightModifier < Answer.Count) && Answer[_questionCounter + RightModifier] != null && Answer[_questionCounter + RightModifier] != "" && Answer[_questionCounter + RightModifier] != " ")
        {
            if (_pictureList[_questionCounter + RightModifier] == null)
            {
                _rightCardImage.color = new Color32(168, 205, 57, 255);
                _rightCardImage.sprite = null;
            }
            else
            {
                _rightCardImage.sprite = _pictureList[_questionCounter + RightModifier];
                _rightCardImage.color = new Color32(204, 204, 204, 255);
            }
            _rightCardInput.gameObject.SetActive(false);
        }
        else
        {
            _rightCardImage.sprite = _cardBackground;
            _rightCardImage.color = new Color32(204, 204, 204, 255);
            _rightCardInput.gameObject.SetActive(true);
        }

        //Far Right Card
        if ((_questionCounter + FarRightModifier < Answer.Count) && Answer[_questionCounter + FarRightModifier] != null && Answer[_questionCounter + FarRightModifier] != "" && Answer[_questionCounter + FarRightModifier] != " ")
        {
            if (_pictureList[_questionCounter + FarRightModifier] == null)
            {
                _farRightCardImage.color = new Color32(168, 205, 57, 255);
                _farRightCardImage.sprite = null;
            }
            else
            {
                _farRightCardImage.sprite = _pictureList[_questionCounter + FarRightModifier];
                _farRightCardImage.color = new Color32(204, 204, 204, 255);
            }
            _farRightCardInput.gameObject.SetActive(false);
        }
        else
        {
            _farRightCardImage.sprite = _cardBackground;
            _farRightCardImage.color = new Color32(204, 204, 204, 255);
            _farRightCardInput.gameObject.SetActive(true);
        }

        //Card 6
        if ((_questionCounter + Card6Modifier < Answer.Count) && Answer[_questionCounter + Card6Modifier] != null && Answer[_questionCounter + Card6Modifier] != "" && Answer[_questionCounter + Card6Modifier] != " ")
        {
            if (_pictureList[_questionCounter + Card6Modifier] == null)
            {
                _card6Image.color = new Color32(168, 205, 57, 255);
                _card6Image.sprite = null;
            }
            else
            {
                _card6Image.sprite = _pictureList[_questionCounter + Card6Modifier];
                _card6Image.color = new Color32(204, 204, 204, 255);
            }
            _card6Input.gameObject.SetActive(false);
        }
        else
        {
            _card6Image.sprite = _cardBackground;
            _card6Image.color = new Color32(204, 204, 204, 255);
            _card6Input.gameObject.SetActive(true);
        }

        //Fake Right Card
        if ((_questionCounter + FakeRightCardModifier < Answer.Count) && Answer[_questionCounter + FakeRightCardModifier] != null && Answer[_questionCounter + FakeRightCardModifier] != "" && Answer[_questionCounter + FakeRightCardModifier] != " ")
        {
            if (_pictureList[_questionCounter + FakeRightCardModifier] == null)
            {
                _fakeRightCardImage.color = new Color32(168, 205, 57, 255);
                _fakeRightCardImage.sprite = null;
            }
            else
            {
                _fakeRightCardImage.sprite = _pictureList[_questionCounter + FakeRightCardModifier];
                _fakeRightCardImage.color = new Color32(204, 204, 204, 255);
            }
            _fakeRightCardInput.gameObject.SetActive(false);
        }
        else
        {
            _fakeRightCardImage.sprite = _cardBackground;
            _fakeRightCardImage.color = new Color32(204, 204, 204, 255);
            _fakeRightCardInput.gameObject.SetActive(true);
        }

        ResetColor();

        switch (_questionCounter % 6)
        {
            case 0:
                _Card1Circle.color = new Color32(246, 142, 31, 255);
                break;
            case 1:
                _Card2Circle.color = new Color32(246, 142, 31, 255);
                break;
            case 2:
                _Card3Circle.color = new Color32(246, 142, 31, 255);
                break;
            case 3:
                _Card4Circle.color = new Color32(246, 142, 31, 255);
                break;
            case 4:
                _Card5Circle.color = new Color32(246, 142, 31, 255);
                break;
            case 5:
                _Card6Circle.color = new Color32(246, 142, 31, 255);
                break;
            default:
                break;
        }
    }

    /*public void GetNames()
    {
        _searchBox.GetComponent<NameSearch>().UpdateList();

        string filter = string.Format("eventid eq '{0}'", GameInformationObject.GetComponent<GlobalInformation>().EventId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, GettingNames);
    }

    void GettingNames(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            _playerList.Clear();

            foreach (var item in list)
            {
                if (item.IsHost == false && item.UniqueId != _uniqueId)
                {
                    _playerList.Add(item);
                }

                if (_player.Name == null)
                {
                    if (item.UniqueId == _uniqueId)
                    {
                        _player = item;
                    }
                }
            }

            _checkedAzure = true;
        }
    }*/

    public void StartGetNames()
    {
        _removedHost = false;
        _searchBox.GetComponent<NameSearch>().UpdateList();
        _playerList.Clear();
        GetNames();
    }

    private void GetNames()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);
        CustomQuery query = new CustomQuery(filter, "eventid desc", 50, _skip);
        _table.Query<NestedResults<MeshworkingTable>>(query, GettingNames); //Query(query);
    }

    private void GettingNames(IRestResponse<NestedResults<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Debug.Log("OnReadNestedResultsCompleted: " + response.ResponseUri + " data: " + response.Content);
            List<MeshworkingTable> items = response.Data.results;
            _totalCount = response.Data.count;
            Debug.Log("Read items count: " + items.Count + "/" + response.Data.count);
            _isPaginated = true; // nested query will support pagination
            if (_skip != 0)
            {
                _playerList.AddRange(items); // append results for paginated results
            }
            else
            {
                _playerList = items; // set for first page of results
            }
            HasNewData = true;

            /*
            foreach (var item in _playerList)
            {
                if (item.IsHost == true)
                {
                    _playerList.Remove(item);

                    if (_player.Name == null)
                    {
                        if (item.UniqueId == _uniqueId)
                        {
                            _player = item;
                        }
                    }
                }
            }*/
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }

        if (_playerList.Count != _totalCount)
        {
            _skip += 50;
            GetNames();
        }
        else
        {
            _isLoadingNextPage = false; // allows next page to be loaded
            _checkedAzure = true;
            _skip = 0;
        }
    }
    
    void SendResults()
    {
        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _statsTable.Query<Meshworking_Results_Participant>(query, SendingResults);
    }

    void SendingResults(IRestResponse<List<Meshworking_Results_Participant>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<Meshworking_Results_Participant> list = response.Data;

            Meshworking_Results_Participant entry = new Meshworking_Results_Participant();
            entry.EventId = _eventId;
            entry.Name = _name;
            entry.UniqueId = _uniqueId;
            entry.CardsCompleted = _cardsCompleted;
            entry.Photostaken = _photoCounter;
            entry.TotalTime = _totalTime;

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    if (item.UniqueId == entry.UniqueId)
                    {
                        item.CardsCompleted = item.CardsCompleted + (entry.CardsCompleted - _previousCardsCompleted);
                        item.Photostaken = item.Photostaken + (entry.Photostaken - _previousPhotoCounter);
                        item.TotalTime = item.TotalTime + (entry.TotalTime - _previousTotalTime);

                        _statsTable.Update<Meshworking_Results_Participant>(item);
                    }
                }
            }
            else
            {
                _statsTable.Insert<Meshworking_Results_Participant>(entry);
            }

            _previousCardsCompleted = entry.CardsCompleted;
            _previousPhotoCounter = entry.Photostaken;
            _previousTotalTime = entry.TotalTime;
        }
    }

    void LeaveGame()
    {
        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, LeavingGame);
    }

    void LeavingGame(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    item.StartGame = false;
                    item.InGame = false;

                    _table.Update<MeshworkingTable>(item);
                }
            }

            _leftGame = true;
        }
    }

    IEnumerator DownloadImages()
    {
        foreach (var item in testlist)
        {
            UM_Camera.Instance.SaveImageToGalalry(item);
        }

        yield return null;
    }
}
