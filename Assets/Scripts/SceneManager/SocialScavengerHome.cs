﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity3dAzure.AppServices;
using RestSharp;
using Pathfinding.Serialization.JsonFx;

public class SocialScavengerHome : MonoBehaviour
{
    [SerializeField] private GameObject _areYouSurePanel;
    private GameObject GameInformationObject;

    [SerializeField] private Button _noButton;
    [SerializeField] private Button _yesButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _videoButton;

    [SerializeField] private Text _eventNameText;
    [SerializeField] private Text _eventIdText;

    private bool _startGame = false;
    private bool _refreshOnce = false;
    private bool _animOnce = true;
    private bool _gotPlayerCount = false;
    private bool _gotFinishedCount = false;
    private bool _doneFading = false;
    private bool _doneInserting = false;
    private bool _singlePerson = true;

    private string _uniqueId;
    private string _yourName;
    private string _eventId;
    private string _email;

    private int _refreshRate = 10;
    private float _white = 1;
    private float _transitionSpeed = .075f;
    private float _playerCount = 0;
    private float _finishedCount = 0;

    private DateTime _startTime;
    private TimeSpan _displayTime;

    private GameObject[] childObjects;

    private MeshworkingTable player = new MeshworkingTable();
    private MeshworkingTable _host = new MeshworkingTable();

    private MobileServiceTable<MeshworkingTable> _table;

    private uint _skip = 0; // no of records to skip
    private uint _totalCount = 0; // count value should be > 0 to paginate

    private bool _isPaginated = false; // only enable infinite scrolling for paginated results
    private bool HasNewData = false; // to reload table view when data has changed
    private bool _isLoadingNextPage = false; // load once when scroll buffer is hit

    // Use this for initialization
    void Start()
    {
        GameInformationObject = GameObject.Find("GameInformation");

        _uniqueId = GameInformationObject.GetComponent<GlobalInformation>().UniqueId;
        _yourName = GameInformationObject.GetComponent<GlobalInformation>().PlayerFirstName + " " + GameInformationObject.GetComponent<GlobalInformation>().PlayerLastName;
        _eventId = GameInformationObject.GetComponent<GlobalInformation>().EventId;
        _email = GameInformationObject.GetComponent<GlobalInformation>().PlayerEmail;

        _eventIdText.text = _eventId;

        GameInformationObject.GetComponent<GlobalInformation>().CameFromGame = true;

        _table = GameInformationObject.GetComponent<AppServiceManager>().Table;

        if (_uniqueId == null || _uniqueId == "")
        {
            _uniqueId = Guid.NewGuid().ToString();
            GameInformationObject.GetComponent<GlobalInformation>().UniqueId = _uniqueId;
            PlayerPrefs.SetString("PlayerAssignedId", _uniqueId);
        }

        _eventNameText.text = GameInformationObject.GetComponent<GlobalInformation>().EventName;

        player.UniqueId = _uniqueId;
        player.EventId = _eventId;
        player.Name = _yourName;
        player.PhotosTaken = 0;
        player.CardsCompleted = 0;
        player.IsHost = false;
        player.Email = _email;

        InsertInfoToAzure();

        _backButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(true);
        });

        _noButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(false);
        });

        _videoButton.onClick.AddListener(() => Application.OpenURL("http://www.mobilemeshevents.com/htp"));

    }

    void Update()
    {
        if (_startGame && _doneInserting)
        {
            if (GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().CameFromGame == true)
            {
                _startGame = false;
                _animOnce = false;
            }

            if (_animOnce)
            {
                _animOnce = false;
                GameInformationObject.GetComponent<SocialScavengerPlayerInformation>().UseSinglePerson = _singlePerson;
                GetComponent<CanvasManager>().OnSocialScavengerGameBoardButtonClicked();
            }
        }

        if (_startGame == false)
        {
            if (Time.time % _refreshRate >= 0 && Time.time % _refreshRate <= .5f)
            {
                //make it only refresh once
                if (_refreshOnce)
                {
                    if (_host.Name == null)
                    {
                        CheckForReady();
                    }
                    else
                    {
                        CheckHostForReady();
                    }
                    _refreshOnce = false;
                }
            }
            else if (Time.time % _refreshRate > .5f)
            {
                _refreshOnce = true;
            }
        }
    }

    void OnDestroy()
    {

        _backButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(true);
        });

        _noButton.onClick.AddListener(() =>
        {
            _areYouSurePanel.SetActive(false);
        });
    }
/*
    void CheckForReady()
    {
        string filter = string.Format("eventid eq '{0}'", GameInformationObject.GetComponent<GlobalInformation>().EventId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, ReadyToCheck);
    }

    public void ReadyToCheck(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {

            bool _hostStarted = false;

            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.IsHost && item.StartGame == true)
                {
                    _hostStarted = true;
                    break;
                }
            }

            if (_hostStarted)
            {
                foreach (var item in list)
                {
                    if (item.UniqueId == _uniqueId)
                    {
                        item.InGame = true;
                        _table.Update<MeshworkingTable>(item);
                        _startGame = true;
                        break;
                    }
                }
            }
        }
    }*/

    void InsertInfoToAzure()
    {
        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, InsertingInfoToAzure);
    }

    void InsertingInfoToAzure(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            bool foundSelf = false;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    foundSelf = true;
                    item.Name = _yourName;
                    item.Email = _email;
                    item.PhotosTaken = 0;
                    item.CardsCompleted = 0;
                    player = item;
                    _table.Update<MeshworkingTable>(player);
                }
            }

            if (foundSelf == false)
            {
                _table.Insert<MeshworkingTable>(player);
            }

            _doneInserting = true;
        }
    }

    private void CheckForReady()
    {
        string filter = string.Format("eventid eq '{0}'", _eventId);
        CustomQuery query = new CustomQuery(filter, "ishost desc", 50, _skip);
        _table.Query<NestedResults<MeshworkingTable>>(query, OnReadNestedResultsCompleted); //Query(query);
    }

    private void OnReadNestedResultsCompleted(IRestResponse<NestedResults<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            bool _hostStarted = false;

            Debug.Log("OnReadNestedResultsCompleted: " + response.ResponseUri + " data: " + response.Content);
            List<MeshworkingTable> items = response.Data.results;
            _totalCount = response.Data.count;
            Debug.Log("Read items count: " + items.Count + "/" + response.Data.count);
            _isPaginated = true; // nested query will support pagination

            HasNewData = true;

            foreach (var item in items)
            {
                if (item.IsHost)
                {
                    _host = item;
                    if (item.StartGame)
                    {
                        _hostStarted = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            
            if (_hostStarted)
            {
                UpdateInfo();                
                _isLoadingNextPage = false; // allows next page to be loaded
                _skip = 0;
            }
            else if(_host.Name == null)
            {
                _skip += 50;
                CheckForReady();
            }
            else if (_hostStarted == false && _host.Name != null)
            {
                CheckHostForReady();
            }
        }
        else
        {
            Debug.Log("Read Nested Results Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }
    }

    void CheckHostForReady()
    {
        string filter = string.Format("uniqueid eq '{0}'", _host.UniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, CheckingHostForReady);
    }

    void CheckingHostForReady(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            bool _hostStarted = false;
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId == _host.UniqueId)
                {
                    if (item.StartGame)
                    {
                        _hostStarted = true;
                        break;
                    }
                }
            }

            if (_hostStarted)
            {
                UpdateInfo();
                _skip = 0;
            }
        }
    }

    void UpdateInfo()
    {       
        string filter = string.Format("uniqueid eq '{0}'", _uniqueId);

        CustomQuery query = new CustomQuery(filter);

        _table.Query<MeshworkingTable>(query, UpdatingInfo);
    }

    void UpdatingInfo(IRestResponse<List<MeshworkingTable>> response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            List<MeshworkingTable> list = response.Data;

            foreach (var item in list)
            {
                if (item.UniqueId == _uniqueId)
                {
                    item.InGame = true;
                    _table.Update<MeshworkingTable>(item);
                    _startGame = true;
                    break;
                }
            }
        }
    }
}
