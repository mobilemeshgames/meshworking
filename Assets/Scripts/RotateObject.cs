﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RotateObject : MonoBehaviour {

    [SerializeField] private float rotationSpeed = 1;


    // Update is called once per frame
    void Update () {

        transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed);

    }
}
