﻿using UnityEngine;
using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using RestSharp;
using Pathfinding.Serialization.JsonFx;
using Unity3dAzure.AppServices;

public class AppServiceManager : MonoBehaviour 
{
    private string _appUrl = "http://meshworking.azurewebsites.net";

    // App Service Rest Client
    private MobileServiceClient _client;

    // App Service Table defined using a DataModel
    public MobileServiceTable<MeshworkingTable> Table;
    public MobileServiceTable<Meshworking_Results_Participant> StatsTable;
    public MobileServiceTable<Rating> RatingTable;
    public MobileServiceTable<RatingHost> RatingHostTable;
    public MobileServiceTable<Survey> SurveyTable;

    // List of Players
    private List<MeshworkingTable> _players = new List<MeshworkingTable>();

	// Use this for initialization
    void Start()
    {
        // Create App Service client (Using factory Create method to force 'https' url)
        _client = MobileServiceClient.Create(_appUrl); //new MobileServiceClient(_appUrl);

        // Get App Service 'Highscores' table
        Table = _client.GetTable<MeshworkingTable>("MeshworkingTable");
        StatsTable = _client.GetTable<Meshworking_Results_Participant>("Meshworking_Results");
        RatingTable = _client.GetTable<Rating>("Rating");
        RatingHostTable = _client.GetTable<RatingHost>("RatingHost");
        SurveyTable = _client.GetTable<Survey>("Survey");
	}

    public void Insert(MeshworkingTable Player)
    {
        Table.Insert<MeshworkingTable>(Player, OnInsertComplete);
    }

    void OnInsertComplete(IRestResponse<MeshworkingTable> response)
    {
        if (response.StatusCode == HttpStatusCode.Created)
        {
            Debug.Log("OnInsertItemCompleted: " + response.Data);
            //Highscore item = response.Data; // if successful the item will have an 'id' property value
            //_score = item;
        }
        else
        {
            Debug.Log("Insert Error Status:" + response.StatusCode + " Uri: " + response.ResponseUri);
        }
    }
}
