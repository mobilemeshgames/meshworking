﻿using System.Collections;
using System.Collections.Generic;
using System;
using Unity3dAzure.AppServices;

[CLSCompliant(false)]
public class Meshworking_Results_Participant : DataModel
{
    //public string Id { get; set; }

    public string UniqueId { get; set; }

    public string EventId { get; set; }

    public string Name { get; set; }

    public int CardsCompleted { get; set; }

    public int Photostaken { get; set; }

    public int TotalTime { get; set; }

    public override string ToString()
    {
        return string.Format("UniqueId: {0} EventId: {1} system properties: {2}", UniqueId, EventId, base.ToString());
    }
}
