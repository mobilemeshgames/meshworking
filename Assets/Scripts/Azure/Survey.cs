﻿using System.Collections;
using System.Collections.Generic;
using System;
using Unity3dAzure.AppServices;

[CLSCompliant(false)]
public class Survey : DataModel
{
    public string UniqueId { get; set; }

    public string Name { get; set; }

    public string Eventid { get; set; }

    public string Email { get; set; }

    public string Question1 { get; set; }

    public string Question2 { get; set; }

    public string Question3 { get; set; }
}
