﻿using System.Collections;
using System.Collections.Generic;
using System;
using Unity3dAzure.AppServices;

[CLSCompliant(false)]
public class MeshworkingTable : DataModel
{
    //public string id { get; set; } // id property is provided when subclassing the DataModel

    public string UniqueId { get; set; }

    public string EventId { get; set; }

    public string Name { get; set; }

    public string Email { get; set; }

    public string EventName { get; set; }

    public bool IsHost { get; set; }

    public bool StartGame { get; set; }

    public bool InGame { get; set; }

    public int CardsCompleted { get; set; }

    public int PhotosTaken { get; set; }

    public override string ToString()
    {
        return string.Format("UniqueId: {0} EventId: {1} system properties: {2}", UniqueId, EventId, base.ToString());
    }
}