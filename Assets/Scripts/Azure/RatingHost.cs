﻿using System.Collections;
using System.Collections.Generic;
using System;
using Unity3dAzure.AppServices;

[CLSCompliant(false)]
public class RatingHost : DataModel
{
    public string UniqueId { get; set; }

    public string Name { get; set; }

    public string Eventid { get; set; }

    public string Email { get; set; }

    public int RatingNum { get; set; }
}
