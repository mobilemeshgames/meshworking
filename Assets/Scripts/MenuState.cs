﻿// This enum is used to track the various states the menu can be in
public enum MenuState 
{  
    None,
    MainMenu, 
    Home,
    SocialScavengerHome,
    SocialScavengerGameBoard,
	Profile,
    HostProfile,
    Code,
    MeshworkingHost,
    Survey,
}