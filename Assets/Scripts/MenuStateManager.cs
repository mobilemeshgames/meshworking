﻿using System.ComponentModel;
using UnityEngine;

public class MenuStateManager : MonoBehaviour
{
    // Keep track of which menu is currently being displayed
    private MenuState _menuState;
    // Keep track of the previous menu shown, this is for certain screens that need to go back to a different screen based on where user came from
    private MenuState _previousMenuState; 

    // 1).
    // Menu prefabs, assigned in the editor, all menu content must be in the UI layer, otherwise the UI camera won't render it
    [SerializeField] private GameObject _canvasBase;
    [SerializeField] private GameObject _canvasHomePrefab;
    [SerializeField] private GameObject _canvasCodePrefab;
    [SerializeField] private GameObject _canvasSocialScavengerHomePrefab;
	[SerializeField] private GameObject _canvasSocialScavengerGameBoardPrefab;
	[SerializeField] private GameObject _canvasProfilePrefab;
    [SerializeField] private GameObject _canvasHostProfilePrefab;
    [SerializeField] private GameObject _meshworkingHostPrefab;
    [SerializeField] private GameObject _surveyPrefab;
	
    // The top menu, since this is probably going to be shared amongst multiple screens, we have it our menu base
    //[SerializeField] private GameObject _header;

    // A reference to our UI camera
    [SerializeField] private Camera _uiCamera;

	void Start () 
    {
        if(_uiCamera == null)
            Debug.LogError("UI Camera has not been set on MenuStateManager!");

        //2).
        // Subscribe to any button click events that cause a new menu to appear
        Messenger.AddListener<MenuState>("back_button_clicked", OnBackButtonClicked);
        Messenger.AddListener("show_main_menu", ShowMainMenu);
        Messenger.AddListener("hide_main_menu", HideMainMenu);
        Messenger.AddListener("home_button_clicked", ShowHomeMenu);
        Messenger.AddListener("profile_button_clicked", ShowProfileMenu);
        Messenger.AddListener("host_profile_button_clicked", ShowHostProfileMenu);
        Messenger.AddListener("code_button_clicked", ShowCodeMenu);
        Messenger.AddListener("socialscavenger_home_button_clicked", ShowSocialScavengerHomeMenu);
        Messenger.AddListener("socialscavenger_gameBoard_button_clicked", ShowSocialScavengerGameBoardMenu);
        Messenger.AddListener("meshworking_host_button_clicked", ShowMeshworkingHostMenu);
        Messenger.AddListener("survey_button_clicked", ShowSurveyMenu);


        // The default state for the menu when the game starts is the main menu (menubase scene default state for now)
        //_menuState = MenuState.MainMenu;
        _menuState = MenuState.Home;

        // This gameObject must never be destroyed when switching scenes
        DontDestroyOnLoad(this.gameObject);

        // Show the sign in menu immediately if we are on the main menu
        ShowHomeMenu();
	}    

    void OnDestroy()
    {
        //3).
        // Unsubscribe to any button click events that cause a new menu to appear
        Messenger.RemoveListener<MenuState>("back_button_clicked", OnBackButtonClicked);
        Messenger.RemoveListener("show_main_menu", ShowMainMenu);
        Messenger.RemoveListener("hide_main_menu", HideMainMenu);
        Messenger.RemoveListener("home_button_clicked", ShowHomeMenu);
        Messenger.RemoveListener("profile_button_clicked", ShowProfileMenu);
        Messenger.RemoveListener("host_profile_button_clicked", ShowHostProfileMenu);
        Messenger.RemoveListener("code_button_clicked", ShowCodeMenu);
        Messenger.RemoveListener("socialscavenger_home_button_clicked", ShowSocialScavengerHomeMenu);
        Messenger.RemoveListener("socialscavenger_gameBoard_button_clicked", ShowSocialScavengerGameBoardMenu);
        Messenger.RemoveListener("meshworking_host_button_clicked", ShowMeshworkingHostMenu);
        Messenger.RemoveListener("survey_button_clicked", ShowSurveyMenu);
    }

    // When the back button has been clicked, we need to check what's sitting on top of the stack, and pop that off
    // The value we pop off is the menu state we were previously at
    public void OnBackButtonClicked(MenuState backButtonTarget)
    {
        Debug.Log("OnBackButtonClicked : " + backButtonTarget);

        Messenger.Broadcast("close_all_menus");

        //4).
        // Show the correct menu based on the previous menu state
        // Whenever you add a new menu state, make sure to add it here too
        switch (backButtonTarget)
        {
            case MenuState.MainMenu:
                ShowMainMenu();
                break;

            case MenuState.Home:
                ShowHomeMenu();
                break;

            case MenuState.Code:
                ShowCodeMenu();
                break;

            case MenuState.SocialScavengerHome:
                ShowSocialScavengerHomeMenu();
                break;

			case MenuState.SocialScavengerGameBoard:
                ShowSocialScavengerGameBoardMenu();
				break;

			case MenuState.Profile:
				ShowProfileMenu();
				break;

            case MenuState.HostProfile:
                ShowHostProfileMenu();
                break;

            case MenuState.MeshworkingHost:
                ShowMeshworkingHostMenu();
                break;

            case MenuState.Survey:
                ShowSurveyMenu();
                break;

        }

    }

    public void ShowMainMenu()
    {
        // Show any main menu specific stuff here
        //_header.SetActive(true);

        ShowCanvasBaseChildObjects();

        // Set the state of the menu to the main menu
        _menuState = MenuState.MainMenu;
    }

    private void HideMainMenu()
    {
       // _header.SetActive(false);
    }

    //5).
    
    public void ShowHomeMenu()
    {
        InstantiateMenu(_canvasHomePrefab);
        HideCanvasBaseChildObjects();
        _menuState = MenuState.Home;
    }
    
    public void ShowCodeMenu()
    {
        InstantiateMenu(_canvasCodePrefab);
        HideCanvasBaseChildObjects();
        _menuState = MenuState.Code;
    }

    public void ShowSocialScavengerHomeMenu()
    {
        InstantiateMenu(_canvasSocialScavengerHomePrefab);
        HideCanvasBaseChildObjects();
        _menuState = MenuState.SocialScavengerHome;
    }

    public void ShowSocialScavengerGameBoardMenu()
	{
        InstantiateMenu(_canvasSocialScavengerGameBoardPrefab);
		HideCanvasBaseChildObjects();
        _menuState = MenuState.SocialScavengerGameBoard;
	}

	public void ShowProfileMenu()
	{
		InstantiateMenu(_canvasProfilePrefab);
		HideCanvasBaseChildObjects();
		_menuState = MenuState.Profile;
	}

    public void ShowHostProfileMenu()
    {
        InstantiateMenu(_canvasHostProfilePrefab);
        HideCanvasBaseChildObjects();
        _menuState = MenuState.HostProfile;
    }

    public void ShowMeshworkingHostMenu() 
    { 
        InstantiateMenu(_meshworkingHostPrefab); 
        HideCanvasBaseChildObjects(); 
        _menuState = MenuState.MeshworkingHost; 
    }

    public void ShowSurveyMenu()
    {
        InstantiateMenu(_surveyPrefab);
        HideCanvasBaseChildObjects();
        _menuState = MenuState.Survey;
    }

    // Generic code to show and initialise a menu
    void InstantiateMenu(GameObject menuPrefab)
    {
        _previousMenuState = _menuState;

        // Make sure we close all other open menus by broadcasting an event
        Messenger.Broadcast("close_all_menus");
        Messenger.Broadcast("hide_main_menu");
        
        GameObject.Instantiate(menuPrefab);

        // Since each menu prefab has it's own Canvas, we need to make sure that since we are using Screen Space - Camera, 
        // that the Canvas is using our UI Camera
        var canvas = menuPrefab.gameObject.GetComponent<Canvas>();

        if (canvas == null)
        {
            Debug.LogError("The menu prefab you have tried to add does not have a Canvas attached to the root object!");
            return;
        }

        // Set the Canvas camera to be the UI camera
        canvas.worldCamera = _uiCamera;
    }

    /// <summary>
    /// This hides all child objects in Canvas base, so they don't appear, or be clickable in other menus
    /// Call this in specific ShowXMenu() methods when you need it
    /// </summary>
    void HideCanvasBaseChildObjects()
    {
        // Disable the CanvasManager when it is not the active canvas, otherwise multiple onBackButtonClick events will fire when broadcasted from the Messenger
        //_canvasBase.GetComponent<CanvasManager>().enabled = false;

        foreach(Transform t in _canvasBase.transform)
            t.gameObject.SetActive(false);
    }

    /// <summary>
    /// This shows all child objects in Canvas base, when the main menu re-appears
    /// This should only be called when the main menu is showwn
    /// </summary>
    void ShowCanvasBaseChildObjects()
    {
        _canvasBase.GetComponent<CanvasManager>().enabled = true;

        foreach(Transform t in _canvasBase.transform)
            t.gameObject.SetActive(true);
    }
}
