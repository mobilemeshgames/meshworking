﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SocialScavengerPlayerInformation : MonoBehaviour {

    public List<string> GlobalAnswerList = new List<string>();
    public List<Sprite> GlobalPictureList = new List<Sprite>();
	[SerializeField] public List<string> preFoundStatements = new List<string>();
	[SerializeField] public List<string> contacts = new List<string>();

    public bool UseSinglePerson = true;
    public bool CameFromGame = false;

	// Use this for initialization
	void Start () 
	{
        preFoundStatements.Add("Find someone who likes dogs more than cats. ");
        preFoundStatements.Add("Find someone who does not like chocolate.");
        preFoundStatements.Add("Find someone who has traveled outside the United States.");
        preFoundStatements.Add("Find someone who likes horror movies. ");
        preFoundStatements.Add("Find someone who likes watching football. ");
        preFoundStatements.Add("Find someone who can not whistle. ");
        preFoundStatements.Add("Find someone who likes cats more than dogs. ");
        preFoundStatements.Add("Find someone who has been in a band. ");
        preFoundStatements.Add("Find someone who has a tattoo. ");
        preFoundStatements.Add("Find someone who has never broken a bone. ");
        preFoundStatements.Add("Find someone who has more than two siblings. ");
        preFoundStatements.Add("Find someone who has worked in the fast food industry. ");
        preFoundStatements.Add("Find someone who can juggle.");
        preFoundStatements.Add("Find someone who has had stitches. ");
        preFoundStatements.Add("Find someone who is afraid of spiders. ");
        preFoundStatements.Add("Find someone who likes sushi.");
        preFoundStatements.Add("Find someone who plays golf.");
        preFoundStatements.Add("Find someone who can speak more than one language.");
        preFoundStatements.Add("Find someone who has been to a professional baseball game. ");
        preFoundStatements.Add("Find someone who can play a musical instrument. ");
        preFoundStatements.Add("Find someone who has never been on an airplane.");
        preFoundStatements.Add("Find someone who has the same middle name as you.");
        preFoundStatements.Add("Find someone who has ridden a horse. ");
        preFoundStatements.Add("Find someone who has the same birthday month as you.");
        preFoundStatements.Add("Find someone who had their tonsils taken out. ");
        preFoundStatements.Add("Find someone who has a twitter account.");
        preFoundStatements.Add("Find someone who has a pet fish.");
        preFoundStatements.Add("Find someone who has swam in an ocean this summer.");
        preFoundStatements.Add("Find someone who is the oldest sibling. ");
        preFoundStatements.Add("Find someone who has the same favorite color as you.");
        preFoundStatements.Add("Find someone who is the youngest sibling. ");
        preFoundStatements.Add("Find someone who is an only child. ");
        preFoundStatements.Add("Find someone who has drives a truck.");
        preFoundStatements.Add("Find someone who doesn’t have any pets. ");
        preFoundStatements.Add("Find someone who loves country music. ");
        preFoundStatements.Add("Find someone who has been to Europe.");
        preFoundStatements.Add("Find someone who owns a motorcycle. ");
        preFoundStatements.Add("Find someone who doesn’t like milk.");
        preFoundStatements.Add("Find someone who has ran a marathon.");
        preFoundStatements.Add("Find someone who had braces.");
        preFoundStatements.Add("Find someone who can salsa dance. ");
        preFoundStatements.Add("Find someone who loves to paint. ");
        preFoundStatements.Add("Find someone who has three or more children. ");
        preFoundStatements.Add("Find someone who was born in a winter month.");
        preFoundStatements.Add("Find someone who was born in a summer month.");
        preFoundStatements.Add("Find someone who has been to Mexico.");
        preFoundStatements.Add("Find someone who works from home.");
        preFoundStatements.Add("Find someone who failed their first driving test.");
        preFoundStatements.Add("Find someone who prefers Pepsi over Coke.");
        preFoundStatements.Add("Find someone who has met someone famous.");


    }

    public void Shuffle()
    {
        for (int i = 0; i < preFoundStatements.Count; i++)
        {
            var temp = preFoundStatements[i];
            int randomIndex = UnityEngine.Random.Range(i, preFoundStatements.Count);
            preFoundStatements[i] = preFoundStatements[randomIndex];
            preFoundStatements[randomIndex] = temp;
        }
    }
}
