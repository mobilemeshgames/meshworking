﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class NameSearch : MonoBehaviour
{

	public List<string> allNames = new List<string> ();
	[SerializeField] private  List<string> displayList = new List<string> ();
	private string fullName;

	[SerializeField] private InputField _inputField;
	[SerializeField] private Button[] buttons = new Button[5];
    [SerializeField] private Button _closeNameSearchButton;

	void Start ()
	{	/*
		allNames.Add ("Steve smith");
		allNames.Add ("lary rodgers");
		allNames.Add ("jeff yass");
		allNames.Add ("jeffery beiber");
		allNames.Add ("jefafa dsfsdf");
		allNames.Add ("connor eth");
		allNames.Add ("connnor esgfsgd");

		allNames.Add ("Nicole gthref");
		allNames.Add ("nick wefg");
		allNames.Add ("StEven yrhgh");
		allNames.Add ("joseph wrgdg");
		allNames.Add ("joe svef");
		allNames.Add ("josh 5gegf");
		allNames.Add ("ashley ge6jhg");
		allNames.Add ("nikki wrgff");
		allNames.Add ("brandon y6hgh");
		allNames.Add ("emily 4wdf");
		allNames.Add ("ashl 5tefg");
		allNames.Add ("bobby rgfdg");		
		allNames.Add ("jen 35gf");
		allNames.Add ("luke j6b5g");		
		allNames.Add ("ben 5gfg5g");
		allNames.Add ("justin 5grgrg");		
		allNames.Add ("selena j7jg");
		allNames.Add ("butt rfsdf");*/

        _closeNameSearchButton.onClick.AddListener(() => gameObject.SetActive(false));

		UpdateList ();

		_inputField.onValueChanged.AddListener (delegate {	UpdateList ();	});
		
	}

	void OnDestroy ()
	{
		_inputField.onValueChanged.RemoveListener (delegate {	UpdateList ();	});
	}

	public void UpdateList ()
	{
		displayList.Clear ();

		if (_inputField.GetComponent<InputField> ().text == "") {														//if nothing is input
			//do and display nothing
		} else {
			for (int i = 0; i < allNames.Count; i++) {																	//loop through each name in the list of all names
				if (allNames [i].ToLower ().Contains (_inputField.GetComponent<InputField> ().text.ToLower ())) {		//compare it to the input text
					displayList.Add (allNames [i]);	
				}
			}
		}

		for (int n = 0; n < 5; n++) {																					//only display the top 5 names in the display list
			if (n >= displayList.Count) {																				//hide the button if there are less than 5 search results...
				buttons [n].gameObject.SetActive (false);																//...and continue
				continue;
			} else {
				buttons [n].gameObject.SetActive (true);																//show the button if we have a name to display
				buttons [n].transform.GetChild (0).GetComponent<Text> ().text = displayList [n];						//display that name in the text child object of the button
			}
		}
	}


	public void buttonClicked(int index){
		fullName = buttons [index].transform.GetChild (0).GetComponent<Text> ().text;;
		_inputField.GetComponent<InputField> ().text = fullName;
        gameObject.SetActive(false);
	}



}


